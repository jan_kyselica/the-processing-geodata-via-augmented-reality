﻿namespace Geodata
{
    partial class fullscreenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imgFullscreen = new Emgu.CV.UI.ImageBox();
            this.txtFPS = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgFullscreen)).BeginInit();
            this.SuspendLayout();
            // 
            // imgFullscreen
            // 
            this.imgFullscreen.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.imgFullscreen.Location = new System.Drawing.Point(13, 10);
            this.imgFullscreen.Margin = new System.Windows.Forms.Padding(15);
            this.imgFullscreen.Name = "imgFullscreen";
            this.imgFullscreen.Size = new System.Drawing.Size(1280, 960);
            this.imgFullscreen.TabIndex = 3;
            this.imgFullscreen.TabStop = false;
            // 
            // txtFPS
            // 
            this.txtFPS.AutoSize = true;
            this.txtFPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtFPS.Location = new System.Drawing.Point(1220, 936);
            this.txtFPS.Name = "txtFPS";
            this.txtFPS.Size = new System.Drawing.Size(44, 20);
            this.txtFPS.TabIndex = 4;
            this.txtFPS.Text = "FPS:";
            // 
            // fullscreenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1294, 977);
            this.Controls.Add(this.txtFPS);
            this.Controls.Add(this.imgFullscreen);
            this.Name = "fullscreenForm";
            this.Text = "Game projection";
            ((System.ComponentModel.ISupportInitialize)(this.imgFullscreen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Emgu.CV.UI.ImageBox imgFullscreen;
        private System.Windows.Forms.Label txtFPS;
    }
}