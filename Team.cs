﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Emgu.CV.Structure;

namespace Geodata
{
    class Team
    {
        public Color Color { get; set; }
        public Bgr BodyColor { get; set; }
        public Bgr FovColor { get; set; }
        public Bgr MovementColor { get; set; }
        public Bgr DeathColor { get; set; }

        public Team(Color color)
        {
            this.Color = color;
            this.BodyColor = new Bgr(color.B, color.G, color.R);
            Color fov, movement, death;
            if(color.Equals(Color.Blue)) {
                fov = Color.LightCyan;
                movement = Color.LightCyan;                
            }
            else {
                fov = Color.Yellow;
                movement = Color.Yellow;                
            }
            death = Color.DarkGray;
            this.DeathColor = new Bgr(death);
            this.FovColor = new Bgr(fov);
            this.MovementColor = new Bgr(movement);            
        }
    }
}
