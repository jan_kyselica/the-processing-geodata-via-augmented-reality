﻿namespace Geodata
{
    partial class LoadDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadDataForm));
            this.radioFromUrl = new System.Windows.Forms.RadioButton();
            this.radioFromFiles = new System.Windows.Forms.RadioButton();
            this.txtTiffUrl = new System.Windows.Forms.RichTextBox();
            this.btnTiff = new System.Windows.Forms.Button();
            this.btnPng = new System.Windows.Forms.Button();
            this.txtTiff = new System.Windows.Forms.TextBox();
            this.txtPng = new System.Windows.Forms.TextBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPngUrl = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // radioFromUrl
            // 
            this.radioFromUrl.AutoSize = true;
            this.radioFromUrl.Checked = true;
            this.radioFromUrl.Location = new System.Drawing.Point(12, 12);
            this.radioFromUrl.Name = "radioFromUrl";
            this.radioFromUrl.Size = new System.Drawing.Size(73, 17);
            this.radioFromUrl.TabIndex = 0;
            this.radioFromUrl.TabStop = true;
            this.radioFromUrl.Text = "From URL";
            this.radioFromUrl.UseVisualStyleBackColor = true;
            this.radioFromUrl.CheckedChanged += new System.EventHandler(this.radioFromUrl_CheckedChanged);
            // 
            // radioFromFiles
            // 
            this.radioFromFiles.AutoSize = true;
            this.radioFromFiles.Location = new System.Drawing.Point(12, 279);
            this.radioFromFiles.Name = "radioFromFiles";
            this.radioFromFiles.Size = new System.Drawing.Size(69, 17);
            this.radioFromFiles.TabIndex = 1;
            this.radioFromFiles.Text = "From files";
            this.radioFromFiles.UseVisualStyleBackColor = true;
            this.radioFromFiles.CheckedChanged += new System.EventHandler(this.radioFromFiles_CheckedChanged);
            // 
            // txtTiffUrl
            // 
            this.txtTiffUrl.Location = new System.Drawing.Point(13, 53);
            this.txtTiffUrl.Name = "txtTiffUrl";
            this.txtTiffUrl.Size = new System.Drawing.Size(521, 85);
            this.txtTiffUrl.TabIndex = 2;
            this.txtTiffUrl.Text = resources.GetString("txtTiffUrl.Text");
            // 
            // btnTiff
            // 
            this.btnTiff.Location = new System.Drawing.Point(459, 300);
            this.btnTiff.Name = "btnTiff";
            this.btnTiff.Size = new System.Drawing.Size(75, 23);
            this.btnTiff.TabIndex = 3;
            this.btnTiff.Text = "Import TIFF";
            this.btnTiff.UseVisualStyleBackColor = true;
            this.btnTiff.Click += new System.EventHandler(this.btnTiff_Click);
            // 
            // btnPng
            // 
            this.btnPng.Location = new System.Drawing.Point(459, 329);
            this.btnPng.Name = "btnPng";
            this.btnPng.Size = new System.Drawing.Size(75, 23);
            this.btnPng.TabIndex = 4;
            this.btnPng.Text = "Import PNG";
            this.btnPng.UseVisualStyleBackColor = true;
            this.btnPng.Click += new System.EventHandler(this.btnPng_Click);
            // 
            // txtTiff
            // 
            this.txtTiff.Enabled = false;
            this.txtTiff.Location = new System.Drawing.Point(15, 302);
            this.txtTiff.Name = "txtTiff";
            this.txtTiff.Size = new System.Drawing.Size(438, 20);
            this.txtTiff.TabIndex = 5;
            // 
            // txtPng
            // 
            this.txtPng.Enabled = false;
            this.txtPng.Location = new System.Drawing.Point(15, 332);
            this.txtPng.Name = "txtPng";
            this.txtPng.Size = new System.Drawing.Size(438, 20);
            this.txtPng.TabIndex = 6;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(459, 370);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 7;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "TIFF";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "PNG";
            // 
            // txtPngUrl
            // 
            this.txtPngUrl.Location = new System.Drawing.Point(12, 169);
            this.txtPngUrl.Name = "txtPngUrl";
            this.txtPngUrl.Size = new System.Drawing.Size(521, 85);
            this.txtPngUrl.TabIndex = 10;
            this.txtPngUrl.Text = resources.GetString("txtPngUrl.Text");
            // 
            // LoadDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 405);
            this.Controls.Add(this.txtPngUrl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.txtPng);
            this.Controls.Add(this.txtTiff);
            this.Controls.Add(this.btnPng);
            this.Controls.Add(this.btnTiff);
            this.Controls.Add(this.txtTiffUrl);
            this.Controls.Add(this.radioFromFiles);
            this.Controls.Add(this.radioFromUrl);
            this.Name = "LoadDataForm";
            this.Text = "Load Data";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioFromUrl;
        private System.Windows.Forms.RadioButton radioFromFiles;
        private System.Windows.Forms.RichTextBox txtTiffUrl;
        private System.Windows.Forms.Button btnTiff;
        private System.Windows.Forms.Button btnPng;
        private System.Windows.Forms.TextBox txtTiff;
        private System.Windows.Forms.TextBox txtPng;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox txtPngUrl;
    }
}