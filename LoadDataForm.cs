﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geodata
{
    public partial class LoadDataForm : Form
    {
        private Form parentForm;        

        public LoadDataForm(Form parentForm)
        {
            InitializeComponent();
            this.parentForm = parentForm;            
        }

        private void btnTiff_Click(object sender, EventArgs e)
        {
            string filename = "";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "TIFF or GEOTIFF|*.TIFF";
            DialogResult dr=ofd.ShowDialog();
 
            if (dr == DialogResult.OK)
            {
               filename = ofd.FileName;
            }
            if (filename != "")
            {
                this.txtTiff.Text = filename;
                Files.TiffPath = filename;
                this.validate();
            }
        }

        private void validate()
        {
            if (this.radioFromUrl.Checked)
            {                
                Uri uriResult;
                bool validPngUrl = Uri.TryCreate(this.txtPngUrl.Text, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
                bool validTiffUrl = Uri.TryCreate(this.txtTiffUrl.Text, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
                if (validPngUrl && validTiffUrl)
                {
                    this.btnLoad.Enabled = true;
                }
                else
                {
                    this.btnLoad.Enabled = false;
                }
            }
            else if (this.radioFromFiles.Checked)
            {
                if (this.txtPng.Text != "" && this.txtTiff.Text != "")
                {
                    this.btnLoad.Enabled = true;
                }
                else
                {
                    this.btnLoad.Enabled = false;
                }
            }
        }
                
        private void btnPng_Click(object sender, EventArgs e)
        {
            string filename = "";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "PNG images|*.PNG";
            DialogResult dr = ofd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                filename = ofd.FileName;
            }
            if (filename != "")
            {
                this.txtPng.Text = filename;
                Files.PngPath = filename;
                this.validate();
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {           
            Cursor.Current = Cursors.WaitCursor;

            // enable model button in menu
            Control[] cntrls = this.parentForm.Controls.Find("menu", true);
            ToolStrip toolStrip = (ToolStrip)cntrls[0];
            var modelButton = toolStrip.Items.Find("btnModel", true);
            modelButton[0].Enabled = true;
            
            if (this.radioFromUrl.Checked)
            {
                WMS.TiffUrl = this.txtTiffUrl.Text;
                WMS.PngUrl = this.txtPngUrl.Text;             
            }
            
            this.Close();            
        }

        private void radioFromUrl_CheckedChanged(object sender, EventArgs e)
        {
            validate();
        }

        private void radioFromFiles_CheckedChanged(object sender, EventArgs e)
        {
            validate();
        }            
    }
}
