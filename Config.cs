﻿using System.Drawing;
using System.IO;

namespace Geodata
{
    static class Config
    {
        // General
        public static bool DetectPlayers { get; set; }
        public static bool CompareHeights { get; set; }
        public static bool HighlightObstacles { get; set; }

        // default markers
        public static string BlueMarkerPath = Path.GetFullPath("images/mark.png");
        public static string GreenMarkerPath = Path.GetFullPath("images/mark1.png");

        // color frame sizes
        public const int ColorWidth = 640;  
        public const int ColorHeight = 480; 

        // depth frame sizes
        public const int DepthWidth = 640;
        public const int DepthHeight = 480;            

        // Constant for player        
        public const double IsEqualsRadius = 100;
        public const int MaxSkippedPlayerFrames = 20;
        public const int MaxSkippedGameFrames = 1;
        public static int NumberOfPlayers { get; set; }
        public static int ShotDispersion { get; set; }
        public static int MoveableRadius { get; set; }
        
        //Playground 
        public static int SizeOfShot { get; set; }

        public const int SurfaceDist = 4000;
        public static int HeightOfModel { get; set; }
        public static int MaxObstaclesArea { get; set; }
        public static int MinObstaclesArea { get; set; }
        public static int HeightOfObstacles { get; set; }

        public static int HeightOfPlayerView { get; set; }
        public static int MinPlayerArea { get; set; }
        public static int MaxPlayerArea { get; set; }

        public static int VisibilityDeviation { get; set; }

        // modeling
        public static int ElevationDeviation { get; set; }
        public static Color LowColor { get; set; }
        public static Color HighColor { get; set; }
    }
}
