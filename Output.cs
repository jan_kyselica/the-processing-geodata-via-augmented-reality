﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.Structure;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.UI;
using System.Drawing.Imaging;

namespace Geodata
{
    public partial class Output : Form
    {
        private int fps = 0;
        private int counter = 0;
        private Game game;        
        private Timer timer;
        private Form fullscreen = new fullscreenForm();        
        private Form loadDataForm;
        private Form loadMarkersForm;
        private bool isModelClicked = false;


        public Output()
        {
            InitializeComponent();
            game = new Game();
            game.loadTemplates();
            timer = new Timer();

            this.initSettings();

            // init Image boxes
            this.initImages();
        }

        private void initSettings()
        {
            // init config
            Config.HeightOfModel = modelHeight.Value;
            Config.MaxObstaclesArea = maxObstacleArea.Value;
            Config.MinObstaclesArea = minObstacleArea.Value;
            Config.HeightOfPlayerView = playerView.Value;
            Config.MaxPlayerArea = maxPlayerArea.Value;
            Config.MinPlayerArea = minPlayerArea.Value;
            Config.ElevationDeviation = deviation.Value;
            Config.LowColor = Color.Fuchsia;
            Config.HighColor = Color.Lime;
            this.boxLowColor.BackColor = Config.LowColor;
            this.boxHighColor.BackColor = Config.HighColor;
            Config.VisibilityDeviation = 5;

            Config.CompareHeights = this.btnCompareHeights.Checked;
            Config.DetectPlayers = this.btnDetectPlayers.Checked;
            Config.HighlightObstacles = this.bthHighlightObstacles.Checked;

            Config.MoveableRadius = this.moveableRadius.Value;
            Config.ShotDispersion = this.shotDispersion.Value;
            Config.SizeOfShot = this.sizeOfShot.Value;

            try
            {
                Config.NumberOfPlayers = Convert.ToInt32(this.numbersOfPlayers.Text);
            }
            catch (FormatException)
            {
                Config.NumberOfPlayers = 2;
            }
        }

        private void Output_Load(object sender, EventArgs e)
        {
            timer.Interval = 1000;
            timer.Start();
            Application.Idle += new EventHandler(show);
            timer.Tick += new EventHandler(tick);

        }

        private void tick(object sender, EventArgs e)
        {
            txtFps.Text = "FPS: " + fps.ToString();
            if (fullscreen.Visible)
            {
                Label FPS = (Label)fullscreen.Controls.Find("txtFPS", true)[0];
                FPS.Text = "FPS: " + fps.ToString();
            }
            fps = 0;
        }

        void show(object sender, EventArgs e)
        {
            fps++;            
            game.play();
            if (fullscreen.Visible)
            {
                updateFullscreen();
            }
            else
            {
                this.setOutput();                
            }            
        }

        private void setOutput()
        {
            Image<Bgr, Byte> img = this.game.getPlayground().Resize(0.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);            
            
            if (game.IO.Heights != null && game.IO.Map!= null)
            {
                this.imgWMS.Image = new Image<Bgr, Byte>(game.IO.Heights).Resize(0.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                this.imgMap.Image = new Image<Bgr, Byte>(game.IO.Map).Resize(0.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                if (Config.CompareHeights)
                {
                    this.imgModeling.Image = game.getHeightDiffs().Resize(0.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                }
            }            

            this.imgDepth.Image = Game.depthDataImage.Resize(0.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            this.imgColor.Image = Game.colorDataImage.Resize(0.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

            if (Notify.show)
            {
                counter++;
                Notify.font.thickness = 1;
                Notify.scale = 0.5f;
                this.imgOutput.Image = Notify.showMessage(img);
                if (counter > Notify.delay)
                {
                    Notify.show = false;
                    counter = 0;
                }
            }
            else
            {
                this.imgOutput.Image = img;
            }
        }

        private void updateFullscreen()
        {
            ImageBox fullScreenImage = (ImageBox)fullscreen.Controls.Find("imgFullscreen", true)[0];
            if (isModelClicked)
            {
                fullScreenImage.Image = game.getHeightDiffs().Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL).Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
            }
            else
            {
                Image<Bgr, Byte> img = game.getPlayground().Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL).Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                if (Notify.show)
                {
                    counter++;
                    Notify.scale = 1.5f;
                    if (game.isEndGame())
                    {
                        Notify.font.thickness = 5;
                        img = Notify.endGame(img);
                    }
                    else
                    {
                        Notify.font.thickness = 3;
                        img = Notify.showMessage(img);
                        if (counter > Notify.delay)
                        {
                            Notify.show = false;
                            counter = 0;
                        }
                    }
                }
                fullScreenImage.Image = img;
            }
        }

        private void Output_Close(object sender, FormClosingEventArgs e)
        {            
            this.game.Dispose();
        }

        private void setSurfaceDistance(object sender, EventArgs e)
        {
            Config.HeightOfModel = modelHeight.Value;
        }

        private void setMaxObstacleArea(object sender, EventArgs e)
        {
            Config.MaxObstaclesArea = maxObstacleArea.Value;
        }

        private void setMinObstacleArea(object sender, EventArgs e)
        {
            Config.MinObstaclesArea = minObstacleArea.Value;
        }

        private void setHeightOfPlayerView(object sender, EventArgs e)
        {
            Config.HeightOfPlayerView = playerView.Value;
        }

        private void setMaxPlayerArea(object sender, EventArgs e)
        {
            Config.MaxPlayerArea = maxPlayerArea.Value;
        }

        private void setMinPlayerArea(object sender, EventArgs e)
        {
            Config.MinPlayerArea = minPlayerArea.Value;
        }

        private void changeNumberOfPlayers(object sender, EventArgs e)
        {
            Config.NumberOfPlayers = numbersOfPlayers.SelectedIndex + 1;
        }

        private void moveableRadius_ValueChanged(object sender, EventArgs e)
        {
            Config.MoveableRadius = moveableRadius.Value;
        }

        private void sizeOfShot_Scroll(object sender, EventArgs e)
        {
            Config.ShotDispersion = shotDispersion.Value;
        }

        private void sizeOfShot_Scroll_1(object sender, EventArgs e)
        {
            Config.SizeOfShot = sizeOfShot.Value;
        }

        private void heightOfObstacle_Scroll(object sender, EventArgs e)
        {
            Config.HeightOfObstacles = heightOfObstacle.Value;
        } 

        private void selectColor(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {                
                var img = (PictureBox)sender;
                img.BackColor = dlg.Color;
                if (img.Name == "boxHighColor")
                {
                    Config.HighColor = dlg.Color;
                }
                else
                {
                    Config.LowColor = dlg.Color;
                }
            }
        }

        private void showFullscreen(object sender, EventArgs e)
        {           
            ToolStripButton clicked = (ToolStripButton)sender;
            if (clicked.Name == "btnModel")
            {
                this.isModelClicked = true;
            }
            else
            {
                this.isModelClicked = false;
            }
            
            this.fullscreen.ShowDialog();           
        }

        private void loadData_click(object sender, EventArgs e)
        {
            loadDataForm = new LoadDataForm(this);            
            loadDataForm.FormClosing += new FormClosingEventHandler(loadDataForm_FormClosing);            
            loadDataForm.ShowDialog();            
        }

        public void loadDataForm_FormClosing(object sender, EventArgs e)
        {
            Control[] ctrls = loadDataForm.Controls.Find("radioFromUrl", true);
            RadioButton radioFromUrl = (RadioButton)ctrls[0]; 
            
            if (radioFromUrl.Checked)
            {                
                if (WMS.TiffUrl != null && WMS.PngUrl != null)
                {
                    this.game.IO = new WMS();
                    loadGeodata();                    
                }
            }
            else
            {
                if (Files.PngPath != null && Files.TiffPath != null)
                {
                    this.game.IO = new Files();
                    loadGeodata();
                }
            }           
        }

        private void loadGeodata()
        {            
            this.game.IO.loadTiff();
            this.game.IO.loadPng();

            // enable modeling options
            groupModeling.Enabled = true;
            
            // enable button for highlighting differences of heights
            btnCompareHeights.Enabled = true;
        }

        private void saveAsPng(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "geodata.png";
            dlg.Filter = "PNG images|*.png";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {               
                this.game.getPlayground().ToBitmap().Save(dlg.FileName, ImageFormat.Png);
            }
        }

        private void saveAsTiff(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "geodata.tiff";
            dlg.Filter = "TIFF files|*.tiff";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // save tiff               
                this.game.IO.saveTiff(Game.depthDataImage.ToBitmap(), dlg.FileName);  
            }
        }

        private void bthHighlightObstacles_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem clicked = (ToolStripMenuItem)sender;            
            if (clicked.Checked)
            {
                Config.HighlightObstacles = false;
                clicked.Checked = false;
                groupSurface.Enabled = false;
            }
            else
            {
                Config.HighlightObstacles = true;
                clicked.Checked = true;
                groupSurface.Enabled = true;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.game = new Game();
            this.game.loadTemplates();

            // init Image boxes
            this.initImages();
        }

        private void setDeviation(object sender, EventArgs e)
        {
            Config.ElevationDeviation = this.deviation.Value;
        }

        private void btnLoadMarkers_Click(object sender, EventArgs e)
        {
            loadMarkersForm = new loadMarkersForm();            
            loadMarkersForm.FormClosing += new FormClosingEventHandler(loadMarkers);            
            loadMarkersForm.ShowDialog();
        }

        void loadMarkers(object sender, EventArgs e)
        {            
            game.loadTemplates();
        }

        private void initImages()
        {
            this.imgMap.Image = Error.flash("Waiting for Png map");
            this.imgDepth.Image = Error.flash("Waiting for depth data");
            this.imgColor.Image = Error.flash("Waiting for color data");
            this.imgOutput.Image = Error.flash("Waiting for data from Kinect");
            this.imgWMS.Image = Error.flash("Waiting for TIFF image");
            this.imgModeling.Image = Error.flash("Waiting for TIFF image");
        }

        private void btnDetectPlayers_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem clicked = (ToolStripMenuItem)sender;
            if (clicked.Checked)
            {
                Config.DetectPlayers = false;
                clicked.Checked = false;
                groupPlayer.Enabled = false;
            }
            else
            {
                Config.DetectPlayers = true;
                clicked.Checked = true;
                groupPlayer.Enabled = true;
            }
        }

        private void btnCompareHeights_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem clicked = (ToolStripMenuItem)sender;
            if (clicked.Checked)
            {
                Config.CompareHeights = false;
                clicked.Checked = false;
                groupModeling.Enabled = false;
            }
            else
            {
                Config.CompareHeights = true;
                clicked.Checked = true;
                groupModeling.Enabled = true;
            }
        }

        private void visibilityDeviation_Scroll(object sender, EventArgs e)
        {
            Config.VisibilityDeviation = visibilityDeviation.Value;
        }

        private void modelHeight_Scroll(object sender, EventArgs e)
        {
            Config.HeightOfModel = modelHeight.Value;
        }                   
    }
}
