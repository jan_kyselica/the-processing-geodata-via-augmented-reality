﻿using System.Drawing;
using BitMiracle.LibTiff.Classic;

namespace Geodata
{
    class Files : IO
    {
        public static string TiffPath = null;
        public static string PngPath = null;        

        public override void loadTiff()
        {
            image = Tiff.Open(Files.TiffPath, "r");            
            this.readRGB();
        }

        public override void loadPng()
        {
            Map = new Bitmap(Files.PngPath);            
        }       
    }
}
