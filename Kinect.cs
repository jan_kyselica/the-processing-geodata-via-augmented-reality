﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;
using Microsoft.Kinect;

namespace Geodata
{
    class Kinect : IDisposable
    {
        private const int BlueIndex = 0;
        private const int GreenIndex = 1;
        private const int RedIndex = 2;

        private KinectSensor sensor;
        private DepthImageFormat depthFormat = DepthImageFormat.Resolution640x480Fps30;        
        private ColorImageFormat colorFormat = ColorImageFormat.RgbResolution640x480Fps30;
        private Bitmap colorBitmap = null;
        private Bitmap depthBitmap = null;
        private Bitmap colorDepthBitmap = null;

        private short[] depthData = null;
        private byte[] colorData = null;        
        private byte[] depthColorData = null;
        public DepthImagePixel[] DepthPixels { get; set; }
        public ColorImagePoint[] ColorCoordinates { get; set; }        
        private int[] colorPixelData;
        public byte[] pixels = null;

        public Kinect()
        {
            // detect Kinect
            if (KinectSensor.KinectSensors.Count == 0)
            {
                MessageBox.Show("No Kinects detected", "Camera Viewer");
                Application.Current.Shutdown();
            }

            try
            {
                sensor = KinectSensor.KinectSensors[0];

                sensor.ColorStream.Enable(colorFormat);
                sensor.DepthStream.Enable(depthFormat);

                sensor.Start();
               
                sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(AllFramesReady);
                this.ColorCoordinates = new ColorImagePoint[this.sensor.DepthStream.FramePixelDataLength];
                this.DepthPixels = new DepthImagePixel[this.sensor.DepthStream.FramePixelDataLength];
                this.colorPixelData = new int[this.sensor.DepthStream.FramePixelDataLength];
            }
            catch
            {
                MessageBox.Show("Kinect initialise failed", "Camera Viewer");
                Application.Current.Shutdown();
            }
        }

        private void AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {            
            using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
            {
                if (depthFrame == null) return;

                if (depthData == null)
                {
                    depthData = new short[depthFrame.PixelDataLength];
                }

                if (depthColorData == null)
                {
                    depthColorData = new byte[depthFrame.PixelDataLength * 4];
                    pixels = new byte[depthFrame.PixelDataLength * 4];
                }

                depthFrame.CopyPixelDataTo(depthData);
                depthFrame.CopyDepthImagePixelDataTo(this.DepthPixels);
                
                // set depth data view
                this.depthColorData = SystemService.showDepthData(depthData, sensor, depthColorData.Length);                                
                Game.depthDataImage = new Image<Bgr, byte>(Converter.ByteArrayToBitmap(depthColorData, depthColorData.Length));
            }

            using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame == null) return;

                if (colorData == null)
                {
                    colorData = new byte[colorFrame.PixelDataLength];
                }

                colorFrame.CopyPixelDataTo(colorData);
            }

            if (this.colorData != null && this.DepthPixels != null)
            {
                this.sensor.CoordinateMapper.MapDepthFrameToColorFrame(this.depthFormat, this.DepthPixels, this.colorFormat, this.ColorCoordinates);                
            }

            this.colorBitmap = Converter.ByteArrayToBitmap(colorData, colorData.Length);
            Game.colorDataImage = new Image<Bgr, byte>(this.colorBitmap);
        }               

        public short[] getDepthData()
        {
            return this.depthData;
        }       

        public Bitmap getColorBitmap()
        {            
            return this.colorBitmap;
        }

        public Bitmap getDepthBitmap()
        {
            return this.depthBitmap;
        }

        public Bitmap getColorDepthBitmap()
        {
            if (this.colorDepthBitmap == null)
            {                
                return new Bitmap(Config.ColorWidth, Config.ColorHeight);
            }
            return this.colorDepthBitmap;
        }                       

        public byte[] getColorByteArray()
        {
            return this.colorData;
        }

        public byte[] getDepthByteArray()
        {
            return this.depthColorData;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.sensor.Stop();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
