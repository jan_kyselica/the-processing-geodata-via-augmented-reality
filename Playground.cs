﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

using Emgu.CV;
using Emgu.Util.TypeEnum;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Windows.Forms;

namespace Geodata
{
    class Playground
    {
        private Bgra moveableAreaColor = new Bgra(Color.Azure.R, Color.Azure.G, Color.Azure.B, 20);

        public Image<Bgr, Byte> BlueMarker;
        public Image<Bgr, Byte> GreenMarker;
        public bool isFire = false;
        public bool showFire = false;  // used for showing fire for two frames
        public CircleF Fire;
        private PointF fireCenter;
        public bool started = false; // detect if game started
        public bool isEnd = false;

        public List<Player> GreenTeam = new List<Player>();
        public List<Player> BlueTeam = new List<Player>();
        public List<MCvBox2D> Obstacles = new List<MCvBox2D>();
        public Contour<Point> contour = null;

        private Color onTurn = Color.Black;

        private int gameCounter = 0;

        public Image<Bgr, Byte> background { get; set; }

        public Playground()
        {
            this.background = null;
        }       

        public void setBackground(Bitmap bmp)
        {
            if (bmp != null)
            {
                this.background = new Image<Bgr, Byte>(bmp).Resize(1, INTER.CV_INTER_LINEAR);
            }
            else
            {
                this.background = Error.flash("No stream!").Resize(2, INTER.CV_INTER_LINEAR);
            }
        }

        /// <summary>
        /// Render players on the background
        /// </summary>
        /// <param name="team"></param>
        public void drawPlayers(ref List<Player> team)
        {
            if (!this.started)
            {
                this.validateIsGameStarted();
            }

            if (team.Any())
            {
                foreach (Player player in team)
                {
                    validatePlayer(player);
                    drawPlayer(player);
                }
            }
            if (this.onTurn != Color.Black)
            {
                this.background.Draw(new Rectangle(new Point(10, 10), new Size(50, 30)), new Bgr(this.onTurn), -1);
            }

            if (this.isFire)
            {
                this.background.Draw(this.Fire, new Bgr(Color.DarkRed), -5);                
                if (this.showFire)
                {                    
                    this.isFire = false;
                    this.showFire = false;
                }
                else
                {
                    this.showFire = true;
                }
            }

            if (this.started && ((this.BlueTeam.Count(player => !player.isDead()) == 0) || (this.GreenTeam.Count(player => !player.isDead()) == 0)))
            {
                this.renderEndGame();
            }

        }

        public void validatePlayer(Player player)
        {

            if (player.FrameCounter > Config.MaxSkippedPlayerFrames && (this.onTurn == Color.Black || this.onTurn == player.team.Color))
            {
                if (!player.isDead())
                {
                    // shot with active player
                    if (player.IsActive)
                    {                        
                        Point point = player.shot();                       
                        if (this.isShotable(point, Point.Round(player.Center)))
                        {                            
                            foreach (Player p in this.getEnemyTeam(player))
                            {                                                          
                                this.isShot(p, point);
                            }
                        }
                        
                        this.Fire = new CircleF(this.fireCenter, Config.SizeOfShot);
                        this.isFire = true;
                        if (this.getEnemyTeam(player).Any())
                        {
                            this.onTurn = this.getEnemyTeam(player).First().team.Color;
                        }
                        player.IsActive = false;
                    }
                    else if (!isActivePlayer() && canActivatePlayer())
                    {
                        player.IsActive = true;                        
                        player.MovementDistance = Config.MoveableRadius;
                    }
                    player.FrameCounter = 0;
                }
            }

            player.FrameCounter++;
        }

        public void drawPlayer(Player player)
        {
            if (player.isDead())
            {
                this.background.Draw(player.Body, player.team.DeathColor, -1);
                double width = SystemService.getDistBetweenTwoPoints((PointF)player.Corners[2], (PointF)player.Corners[3]);
                double height = SystemService.getDistBetweenTwoPoints((PointF)player.Corners[1], (PointF)player.Corners[0]);
                Cross2DF cross = new Cross2DF(player.Center, (float)width, (float)height);                
                this.background.Draw(cross, new Bgr(Color.Red), 4);
            }

            else
            {
                if (player.IsActive)
                {
                    this.background.Draw(new CircleF(player.Center, (float)player.MovementDistance), player.team.MovementColor, 2);
                }
                this.background.DrawPolyline(player.FOV, true, player.team.MovementColor, 3);
                // fill body
                this.background.Draw(player.Body, new Bgr(Color.White), -1);
                // contours
                this.background.Draw(player.Body, player.team.BodyColor, 2);

                //health
                PointF first = player.Body.GetVertices()[2];
                PointF second = player.Body.GetVertices()[1];

                // order of the corners is changed when user rotate with object
                // find which corners are on the left side
                if (player.Corners[2].X < player.Corners[1].X)
                {
                    first = player.Body.GetVertices()[0];
                    second = player.Body.GetVertices()[3];
                }

                double dist = SystemService.getDistBetweenTwoPoints(first, second);
                dist = (player.Health / 100.0) * dist;
                PointF endPoint = SystemService.getPointFInDistance(first, (int)dist, second);
                LineSegment2DF health = new LineSegment2DF(first, endPoint);
                this.background.Draw(health, new Bgr(Color.Red), 9);
            }
        }

        /// <summary>
        /// Detect if player has been hit
        /// </summary>
        /// <param name="player"></param>
        /// <param name="aim"></param>
        public void isShot(Player player, Point aim)
        {   
            if (player.Corners[1].X < player.Corners[2].X)
            {
                if ((aim.X <= player.Corners[2].X && aim.X >= player.Corners[0].X)
                    && aim.Y <= player.Corners[1].Y && aim.Y >= player.Corners[3].Y)
                {
                    player.Health -= shotPlayer();
                }
            }
            else
            {                
                if ((aim.X <= player.Corners[0].X && aim.X >= player.Corners[2].X)
                    && aim.Y <= player.Corners[3].Y && aim.Y >= player.Corners[1].Y)
                {
                    player.Health -= shotPlayer();
                }
            }
        }

        /// <summary>
        /// Get damage and flash message to background
        /// </summary>
        /// <returns></returns>
        private int shotPlayer()
        {           
            int damage = SystemService.random.Next(1, 100);
            Notify.message = "Damage " + damage.ToString();
            Notify.show = true;
            return damage;
        }

        public void addPlayer(Player player, ref List<Player> team)
        {
            team.Add(player);
        }

        /// <summary>
        /// Load images of markers
        /// </summary>
        /// <param name="path"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public bool loadTemplates(string path, ref Image<Bgr, Byte> template)
        {
            if (File.Exists(path))
            {
                template = new Image<Bgr, byte>(path);
                return true;
            }
            else
            {
                MessageBox.Show("Template not found " + path);
            }
            return false;
        }

        private List<Player> getEnemyTeam(Player player)
        {
            if (player.team.Color.Equals(Color.Blue))
            {
                return this.GreenTeam;
            }
            else
            {
                return this.BlueTeam;
            }
        }

        private void renderEndGame()
        {
            Notify.show = true;
            this.isEnd = true;
            if (BlueTeam.Any())
            {
                Notify.message = "Blue team won";
            }
            else
            {
                Notify.message = "Green team won";
            }
        }

        private void validateIsGameStarted()
        {
            if (!this.started && ((this.BlueTeam.Count != 0) && (this.GreenTeam.Count != 0)))
            {
                this.started = true;
            }
        }

        public void renderObstacles()
        {
            Bgr color = new Bgr(Color.IndianRed);
            foreach (MCvBox2D trammel in this.Obstacles)
            {
                Point[] pts = Array.ConvertAll(trammel.GetVertices(), (p => Point.Round(p)));
                this.background.Draw(new LineSegment2D(pts[0], pts[2]), color, 3);
                this.background.Draw(new LineSegment2D(pts[1], pts[3]), color, 3);
                this.background.Draw(trammel, new Bgr(Color.IndianRed), 3);
            }
        }

        /// <summary>
        /// Detect if there is any obstacles between player and enemy
        /// </summary>
        /// <param name="aim"></param>
        /// <param name="shooter"></param>
        /// <returns></returns>
        private bool isShotable(Point aim, Point shooter)
        {
            this.fireCenter = aim;

            // if highlighting of obstacles is disabled, every position is shotable
            if (!Config.HighlightObstacles)
            {
                return true;
            }

            if (this.contour == null || !this.contour.Any())
            {
                return true;
            }
            LineSegment2D line = new LineSegment2D(aim, shooter);
            Contour<Point> contours = this.contour;                        

            for (; contours != null; contours = contours.HNext)
            {
                if ((contours.Area < Config.MaxObstaclesArea) && (contours.Area > Config.MinObstaclesArea))
                {
                    MCvBox2D box = contours.GetMinAreaRect();
                    
                    // obstacle is too far
                    if (box.center.X > line.Length && box.center.Y > line.Length)
                    {
                        continue;
                    }
                    LineSegment2D centerToObstacle = new LineSegment2D(shooter, Point.Round(box.center));
                    PointF pointInObstacle = SystemService.getPointInDistance(shooter, centerToObstacle.Length, Point.Round(box.center));                    
                    double inContour = contours.InContour(pointInObstacle);

                    // if is positive, point is inside of contour, if zero point is on contour
                    if (inContour >= 0.0)
                    {                        
                        this.fireCenter = pointInObstacle;
                        return false;
                    }
                }
            }
            return true;
        }

        public bool isActivePlayer()
        {
            return (this.BlueTeam.Any(p => p.IsActive) || this.GreenTeam.Any(p => p.IsActive));
        }

        public Player getActivePlayer()
        {
            if (this.BlueTeam.Any(p => p.IsActive))
            {
                return this.BlueTeam.First(p => p.IsActive);
            }
            else
            {
                return this.GreenTeam.First(p => p.IsActive);
            }
        }

        /// <summary>
        /// Skip some frames before activate next player
        /// </summary>
        /// <returns></returns>
        private bool canActivatePlayer()
        {
            if (gameCounter < Config.MaxSkippedGameFrames)
            {
                gameCounter++;
                return false;
            }
            gameCounter = 0;
            return true;

        }
    }
}
