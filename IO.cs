﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using BitMiracle.LibTiff.Classic;

namespace Geodata
{
    class IO
    {
        public Bitmap Map { get; set; }
        public Bitmap Heights { get; set; }

        public Tiff image { get; set; }
        public Bitmap bmp { get; set; }
        public byte[,] HeightValues { get; set; }

        public IO()
        {
            Map = null;
            Heights = new Bitmap(Config.ColorWidth, Config.ColorHeight);
            HeightValues = new byte[Config.ColorWidth, Config.ColorHeight];
        }

        public virtual void loadTiff()
        {

        }

        public virtual void loadPng()
        {

        }

        public bool validateMap()
        {
            if (Map.Width != Config.ColorWidth || Map.Height != Config.ColorHeight)
            {
                return false;
            }
            return true;
        }

        public void readRGB()
        {
            if (image == null)
            {
                MessageBox.Show("Could not open incoming image");
                return;
            }

            // Find the width and height of the image
            FieldValue[] value = image.GetField(TiffTag.IMAGEWIDTH);
            int width = value[0].ToInt();

            if (width != Config.ColorWidth)
            {
                MessageBox.Show("Width of TIFF Image must be 480 px");
                return;
            }

            value = image.GetField(TiffTag.IMAGELENGTH);
            int height = value[0].ToInt();

            if (height != Config.ColorHeight)
            {
                MessageBox.Show("Height of TIFF Image must be 640 px");
                return;
            }

            int imageSize = height * width;
            int[] raster = new int[imageSize];

            // Read the image into the memory buffer 
            if (!image.ReadRGBAImage(width, height, raster))
            {
                MessageBox.Show("Could not read image");
                return;
            }
            Color color = new Color();

            for (int i = 0; i < height; ++i)
                for (int j = 0; j < width; ++j)
                {
                    color = getSample(i, j, raster, width, height);
                    Heights.SetPixel(j, i, color);
                    HeightValues[j, i] = getAvg(color);
                }
        }

        protected Color getSample(int x, int y, int[] raster, int width, int height)
        {
            int offset = (height - x - 1) * width + y;
            int red = BitMiracle.LibTiff.Classic.Tiff.GetR(raster[offset]);
            int green = BitMiracle.LibTiff.Classic.Tiff.GetG(raster[offset]);
            int blue = BitMiracle.LibTiff.Classic.Tiff.GetB(raster[offset]);
            return Color.FromArgb(red, green, blue);
        }

        protected byte getAvg(Color color)
        {
            return (byte)((color.R + color.G + color.B) / 3);
        }

        public void saveTiff(Bitmap bmp, String name)
        {
            using (Tiff tif =  Tiff.Open(name, "w"))
            {
                byte[] raster = getImageRasterBytes(bmp, PixelFormat.Format24bppRgb);
                tif.SetField(TiffTag.IMAGEWIDTH, bmp.Width);
                tif.SetField(TiffTag.IMAGELENGTH, bmp.Height);
                tif.SetField(TiffTag.COMPRESSION, Compression.LZW);
                tif.SetField(TiffTag.PHOTOMETRIC, Photometric.RGB);

                tif.SetField(TiffTag.ROWSPERSTRIP, bmp.Height);

                tif.SetField(TiffTag.XRESOLUTION, bmp.HorizontalResolution);
                tif.SetField(TiffTag.YRESOLUTION, bmp.VerticalResolution);

                tif.SetField(TiffTag.BITSPERSAMPLE, 8);
                tif.SetField(TiffTag.SAMPLESPERPIXEL, 3);

                tif.SetField(TiffTag.PLANARCONFIG, PlanarConfig.CONTIG);

                int stride = raster.Length / bmp.Height;
                convertSamples(raster, bmp.Width, bmp.Height);

                for (int i = 0, offset = 0; i < bmp.Height; i++)
                {
                    tif.WriteScanline(raster, offset, i, 0);
                    offset += stride;
                }
            }

            System.Diagnostics.Process.Start(name);
        }        

        private static byte[] getImageRasterBytes(Bitmap bmp, PixelFormat format)
        {
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            byte[] bits = null;

            try
            {
                BitmapData bmpdata = bmp.LockBits(rect, ImageLockMode.ReadWrite, format);                
                bits = new byte[bmpdata.Stride * bmpdata.Height];
                
                System.Runtime.InteropServices.Marshal.Copy(bmpdata.Scan0, bits, 0, bits.Length);               
                bmp.UnlockBits(bmpdata);
            }
            catch
            {
                return null;
            }

            return bits;
        }

        /// <summary> 
        /// Converts BGR samples into RGB samples 
        /// </summary> 
        private static void convertSamples(byte[] data, int width, int height)
        {
            int stride = data.Length / height;
            const int samplesPerPixel = 3;

            for (int y = 0; y < height; y++)
            {
                int offset = stride * y;
                int strideEnd = offset + width * samplesPerPixel;

                for (int i = offset; i < strideEnd; i += samplesPerPixel)
                {
                    byte temp = data[i + 2];
                    data[i + 2] = data[i];
                    data[i] = temp;
                }
            }
        }
    }
}
