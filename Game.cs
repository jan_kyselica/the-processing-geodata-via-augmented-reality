﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;


namespace Geodata
{
    class Game : IDisposable
    {
        private Kinect sensor;
        private Playground playground;
        private Detector detector;

        // main image
        private Image<Bgr, Byte> colorImage;

        // helper images
        public static Image<Bgr, Byte> depthDataImage;
        public static Image<Bgr, Byte> colorDataImage = null;
        public static Image<Bgr, Byte> imgHeightDiff;
        
        public IO IO;

        public Game()
        {
            this.sensor = new Kinect();
            this.playground = new Playground();
            this.detector = new Detector();           
            this.IO = new IO();

            // init images
            Image<Bgr, Byte> img = new Image<Bgr, byte>(Config.ColorWidth, Config.ColorHeight, new Bgr(Color.Aqua));
            Game.depthDataImage = img;
            Game.imgHeightDiff = img;
            Game.colorDataImage = img;
        }

        public void loadTemplates()
        {            
            this.playground.loadTemplates(Config.BlueMarkerPath, ref this.playground.BlueMarker);
            this.playground.loadTemplates(Config.GreenMarkerPath, ref this.playground.GreenMarker);
        }
        
        public void play()
        {
            #region set playground
            Bitmap bmp = this.sensor.getColorBitmap();
            this.playground.setBackground(bmp);

            if (bmp == null)
            {
                // set previous image
                this.colorImage = this.playground.background;
            }
            else
            {
                this.colorImage = new Image<Bgr, byte>(bmp);
            }
            #endregion


            if (Game.colorDataImage == null)
            {
                Game.colorDataImage = this.playground.background;
            }

            try
            {
                // detect markers of players
                if (Config.DetectPlayers)
                {
                    this.detectPlayers();
                }                

                // if loaded data change background to map
                if (this.IO.Map != null)
                {
                    this.playground.background = new Image<Bgr, Byte>(this.IO.Map);
                }
                
                if (this.playground.isActivePlayer())
                {
                    renderVisibility();
                }

                // detect obstacles
                if (Config.HighlightObstacles)
                {
                    this.detectObstacles();
                }
            }
            catch (Exception e)
            {                
                // log exceptions
                Console.WriteLine(e.Message);
                Console.WriteLine(e.Source);
                Console.WriteLine(e.StackTrace);                
            }

            // render images
            this.render();
        }

        /// <summary>
        /// Highlight diffs between tiff image and depth data
        /// </summary>
        /// <returns>Image with highlighted diffs</returns>
        public Image<Bgr, Byte> getHeightDiffs()
        {            
            return new Image<Bgr, byte>(SystemService.calculateDiff(this.IO.HeightValues, this.sensor.getDepthData(), this.sensor.getColorBitmap(), this.sensor.DepthPixels, this.sensor.ColorCoordinates));
        }

        /// <summary>
        /// Detect players using SURF method
        /// </summary>
        private void detectPlayers()
        {            
            List<Player> tempBlueTeam = this.detector.SURFFindTemplate(this.colorImage, this.playground.BlueMarker, Color.Blue);
            if (tempBlueTeam.Any())
            {
                this.compareTeams(ref this.playground.BlueTeam, tempBlueTeam);
            }

            List<Player> tempGreenTeam = this.detector.SURFFindTemplate(this.colorImage, this.playground.GreenMarker, Color.Green);
            if (tempGreenTeam.Any())
            {
                this.compareTeams(ref this.playground.GreenTeam, tempGreenTeam);
            }
        }

        /// <summary> 
        /// Find obstacles on the depth data
        /// </summary>
        private void detectObstacles()
        {
            Image<Bgr, Byte> img = new Image<Bgr, Byte>(Converter.ByteArrayToBitmap(this.sensor.getDepthByteArray(), this.sensor.getDepthByteArray().Length));                     
            this.playground.contour = this.detector.findObstacles(img);
            this.playground.Obstacles = this.detector.obstacle;            
        }

        /// <summary>
        /// Show visible area on the playground
        /// </summary>
        private void renderVisibility()
        {
            if (this.IO.Map != null)
            {
                // create deep copy a and flip
                Bitmap flipped = new Bitmap(this.IO.Map);
                flipped.RotateFlip(RotateFlipType.RotateNoneFlipY);

                byte[] imgData = Converter.BitmapToByteArrayWithoutHeader(flipped);
                Bitmap bm = SystemService.getVisibility(this.sensor.getColorByteArray(), this.sensor.DepthPixels, this.sensor.ColorCoordinates, this.playground.getActivePlayer(), imgData);                
                this.playground.background = new Image<Bgr, byte>(bm);                
            }
        }

        /// <summary>
        /// Render all objects on the background
        /// </summary>
        private void render()
        {
            this.playground.drawPlayers(ref this.playground.BlueTeam);
            this.playground.drawPlayers(ref this.playground.GreenTeam);
            if (Config.HighlightObstacles)
            {
                this.playground.renderObstacles();
            }             
        }

        public Image<Bgr, Byte> getPlayground()
        {
            return this.playground.background;
        }

        /// <summary>
        /// Compare new detected players with current players in the team
        /// </summary>
        /// <param name="team">Current players</param>
        /// <param name="others">New detected players</param>
        public void compareTeams(ref List<Player> team, List<Player> others)
        {
            foreach (Player other in others)
            {
                if (team.Contains(other))
                {
                    Player founded = team.Find(
                        delegate(Player player)
                        {
                            if (player.Equals(other))
                            {
                                player.FrameCounter = 0;
                                return true;
                            }
                            return false;
                        }
                    );


                    if (founded.IsActive)
                    {
                        founded.updateActive(other);                       
                    }
                    // if is there some active player
                    else if (playground.isActivePlayer())
                    {
                        founded.updateOrientation(other);                       
                    }
                    else 
                    {                        
                        founded.update(other);
                    }                     
                }
                else
                {
                    // add new player
                    if ((other.Center.X != 0 && other.Center.Y != 0))
                    {
                        if (team.Count < Config.NumberOfPlayers)
                        {
                            team.Add(other);
                        }
                    }
                }
            }
        }

        public bool isEndGame()
        {
            if (playground.isEnd)
            {
                return true;
            }
            return false;

        }

        // before closing dispose Kinect sensor
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.sensor.Dispose();                
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
