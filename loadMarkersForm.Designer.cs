﻿namespace Geodata
{
    partial class loadMarkersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGreenTemplate = new System.Windows.Forms.Button();
            this.btnBlueTemplate = new System.Windows.Forms.Button();
            this.txtTeamGreen = new System.Windows.Forms.TextBox();
            this.txtTeamBlue = new System.Windows.Forms.TextBox();
            this.btnLoadMarkers = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnGreenTemplate
            // 
            this.btnGreenTemplate.Location = new System.Drawing.Point(459, 51);
            this.btnGreenTemplate.Name = "btnGreenTemplate";
            this.btnGreenTemplate.Size = new System.Drawing.Size(75, 23);
            this.btnGreenTemplate.TabIndex = 0;
            this.btnGreenTemplate.Text = "Choose";
            this.btnGreenTemplate.UseVisualStyleBackColor = true;
            this.btnGreenTemplate.Click += new System.EventHandler(this.btnGreenTemplate_Click);
            // 
            // btnBlueTemplate
            // 
            this.btnBlueTemplate.Location = new System.Drawing.Point(459, 126);
            this.btnBlueTemplate.Name = "btnBlueTemplate";
            this.btnBlueTemplate.Size = new System.Drawing.Size(75, 23);
            this.btnBlueTemplate.TabIndex = 1;
            this.btnBlueTemplate.Text = "Choose";
            this.btnBlueTemplate.UseVisualStyleBackColor = true;
            this.btnBlueTemplate.Click += new System.EventHandler(this.btnBlueTemplate_Click);
            // 
            // txtTeamGreen
            // 
            this.txtTeamGreen.Enabled = false;
            this.txtTeamGreen.Location = new System.Drawing.Point(12, 54);
            this.txtTeamGreen.Name = "txtTeamGreen";
            this.txtTeamGreen.Size = new System.Drawing.Size(427, 20);
            this.txtTeamGreen.TabIndex = 2;
            // 
            // txtTeamBlue
            // 
            this.txtTeamBlue.Enabled = false;
            this.txtTeamBlue.Location = new System.Drawing.Point(12, 129);
            this.txtTeamBlue.Name = "txtTeamBlue";
            this.txtTeamBlue.Size = new System.Drawing.Size(427, 20);
            this.txtTeamBlue.TabIndex = 3;
            // 
            // btnLoadMarkers
            // 
            this.btnLoadMarkers.Location = new System.Drawing.Point(459, 367);
            this.btnLoadMarkers.Name = "btnLoadMarkers";
            this.btnLoadMarkers.Size = new System.Drawing.Size(75, 23);
            this.btnLoadMarkers.TabIndex = 4;
            this.btnLoadMarkers.Text = "Load";
            this.btnLoadMarkers.UseVisualStyleBackColor = true;
            this.btnLoadMarkers.Click += new System.EventHandler(this.btnLoadMarkers_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Marker for green team";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Marker for blue team";
            // 
            // loadMarkersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 405);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLoadMarkers);
            this.Controls.Add(this.txtTeamBlue);
            this.Controls.Add(this.txtTeamGreen);
            this.Controls.Add(this.btnBlueTemplate);
            this.Controls.Add(this.btnGreenTemplate);
            this.Name = "loadMarkersForm";
            this.Text = "Load markers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGreenTemplate;
        private System.Windows.Forms.Button btnBlueTemplate;
        private System.Windows.Forms.TextBox txtTeamGreen;
        private System.Windows.Forms.TextBox txtTeamBlue;
        private System.Windows.Forms.Button btnLoadMarkers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}