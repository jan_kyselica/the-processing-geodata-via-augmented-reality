﻿using System;
using System.Drawing;
using System.Windows;
using Emgu.CV.Structure;
using Microsoft.Kinect;

namespace Geodata
{
    static class SystemService
    {
        public static Random random = new Random();

        public static Bitmap calculateDiff(byte[,] realData, short[] virtualData, Bitmap bmp, DepthImagePixel[] depthPixels, ColorImagePoint[] colorCoordinates)
        {            
            Color color = new Color();

            for (int i = 0; i < Config.ColorHeight; ++i)
                for (int j = 0; j < Config.ColorWidth; ++j)
                {
                    int offset = (i * Config.ColorWidth + j);                    
                    int depthValue = virtualData[offset] >> 3;                  
                                        
                    byte depthByte = (byte)(255 - (depthValue >> 4));                                                            
                    
                    // get color coordinates
                    ColorImagePoint colorPoint = colorCoordinates[offset];
                    int pointInDepthX = colorPoint.X;
                    int pointInDepthY = colorPoint.Y;
                    if (pointInDepthX > 0 && pointInDepthX < Config.DepthWidth && pointInDepthY >= 0 && pointInDepthY < Config.DepthHeight)
                    {                        
                        try
                        {
                            depthByte -= Convert.ToByte(Config.HeightOfModel);
                        }
                        catch
                        {
                            depthByte = 0;
                        }

                        if (realData[j, i] + Config.ElevationDeviation < depthByte)
                        {
                            color = Config.LowColor;                           
                            bmp.SetPixel(pointInDepthX, pointInDepthY, color);
                        }
                        else if (realData[j, i] - Config.ElevationDeviation > depthByte)
                        {
                            color = Config.HighColor;                            
                            bmp.SetPixel(pointInDepthX, pointInDepthY, color);
                        }
                    }
                }
        
            return bmp;
        }                      

        public static byte[] showDepthData(short[] depthData, KinectSensor myKinect, int byteSize)
        {            
            int depthColorImagePos = 0;
            byte[] depthColorImage = new byte[byteSize];
            for (int depthPos = 0; depthPos < depthData.Length; depthPos++)
            {
                int depthValue = depthData[depthPos] >> 3;

                // Check for the invalid values
                if (depthValue == myKinect.DepthStream.UnknownDepth ||
                    depthValue == myKinect.DepthStream.TooFarDepth) {

                    // Color invalid pixels black
                    // Blue
                    depthColorImage[depthColorImagePos] = 0;
                    depthColorImagePos++;
                    // Green
                    depthColorImage[depthColorImagePos] = 0;
                    depthColorImagePos++;
                    // Red
                    depthColorImage[depthColorImagePos] = 0;
                    depthColorImagePos++;
                }
                else if(depthValue == myKinect.DepthStream.TooNearDepth)
                {
                    // Color invalid pixels white
                    // Blue
                    depthColorImage[depthColorImagePos] = 255;
                    depthColorImagePos++;
                    // Green
                    depthColorImage[depthColorImagePos] = 255;
                    depthColorImagePos++;
                    // Red
                    depthColorImage[depthColorImagePos] = 255;
                    depthColorImagePos++;
                }
                else
                {
                    byte depthByte = (byte)(255-(depthValue >> 4));                   

                    // Blue
                    depthColorImage[depthColorImagePos] = depthByte;
                    depthColorImagePos++;
                    // Green
                    depthColorImage[depthColorImagePos] = depthByte;
                    depthColorImagePos++;
                    // Red
                    depthColorImage[depthColorImagePos] = depthByte;
                    depthColorImagePos++;                    
                }
                // transparency
                depthColorImagePos++;
            }
            return depthColorImage;
        }

        public static double getDistBetweenTwoPoints(PointF p1, PointF p2)
        {
            double dx = Math.Pow((double)p1.X - (double)p2.X, 2);
            double dy = Math.Pow((double)p1.Y - (double)p2.Y, 2);
            return Math.Sqrt(dx + dy);
        }          

        public static PointF getNearestPoint(PointF point, PointF[] vertices)
        {
            PointF nearest = new PointF();
            double minDistance = 0;
            foreach (PointF p in vertices)
            {
                double distance = SystemService.getDistBetweenTwoPoints(point, p);
                if (minDistance < distance)
                {
                    minDistance = distance;
                    nearest = p;
                }
            }
            return nearest;
        }

        public static System.Drawing.Point getMidPoint(System.Drawing.Point p1, System.Drawing.Point p2)
        {
            System.Drawing.Point mid = new System.Drawing.Point();
            mid.X = (p1.X + p2.X) / 2;
            mid.Y = (p1.Y + p2.Y) / 2;
            return mid;
        }

        public static System.Drawing.Point getFarPoint(System.Drawing.Point p1, System.Drawing.Point p2)
        {
            System.Drawing.Point fPoint = new System.Drawing.Point();
            fPoint.X = 2 * p2.X - p1.X;
            fPoint.Y = 2 * p2.Y - p1.Y;
            return fPoint;            
        }

        public static System.Drawing.Point getRandomPoint(System.Drawing.Point center)
        {           
            var angle = SystemService.random.NextDouble() * Math.PI * 2;
            var radius = SystemService.random.Next(Config.ShotDispersion) * angle;
            var x = center.X + radius * Math.Cos(angle);
            var y = center.Y + radius * Math.Sin(angle);            
            return new System.Drawing.Point((int)x, (int)y);                        
        }

        public static bool isBox(System.Drawing.Point[] pts)
        {
            double[] angles = new double[4];            
            
            // angle nearby point 0     
            // first line direction 0->3, second line direction 0->1
            angles[0] = SystemService.getAngleBetweenLines(new LineSegment2D(pts[0], pts[3]), new LineSegment2D(pts[0], pts[1]));            
            angles[1] = SystemService.getAngleBetweenLines(new LineSegment2D(pts[1], pts[0]), new LineSegment2D(pts[1], pts[2]));            
            angles[2] = SystemService.getAngleBetweenLines(new LineSegment2D(pts[2], pts[1]), new LineSegment2D(pts[2], pts[3]));            
            angles[3] = SystemService.getAngleBetweenLines(new LineSegment2D(pts[3], pts[2]), new LineSegment2D(pts[3], pts[0]));

            foreach (double angle in angles)
            {
                var tempAngle = Math.Abs(angle);                
                if (tempAngle < 60 || 120 < tempAngle)
                {
                    return false;
                }
            }
            
            return true;
        }        

        public static double getAngleBetweenLines(LineSegment2D line1, LineSegment2D line2)
        {
            System.Drawing.Point vector1 = new System.Drawing.Point(line1.P1.X - line1.P2.X, line1.P1.Y - line1.P2.Y);
            System.Drawing.Point vector2 = new System.Drawing.Point(line2.P2.X - line2.P1.X, line2.P2.Y - line2.P1.Y);
            
            double men = (vector1.X * vector2.X) + (vector1.Y * vector2.Y);
            double cit = (Math.Sqrt((Math.Pow(vector1.X, 2) + Math.Pow(vector1.Y, 2))) *  (Math.Sqrt((Math.Pow(vector2.X, 2)) + Math.Pow(vector2.Y, 2))));
            
            return Math.Acos(men / cit) * (180/Math.PI);
        }

        public static PointF getPointInDistance(System.Drawing.Point start, double dist, System.Drawing.Point end)
        {
            // get vector
            int[] vect = new int[2];
            vect[0] = end.X - start.X;
            vect[1] = end.Y - start.Y;
            
            double vecSize = Math.Sqrt(Math.Pow(vect[0], 2) + Math.Pow(vect[1], 2));
            double[] uVect = new double[2];
            uVect[0] = vect[0] / vecSize;
            uVect[1] = vect[1] / vecSize;

            PointF point = new System.Drawing.Point();
            point.X = (float)(start.X + (dist*uVect[0]));
            point.Y = (float)(start.Y + (dist*uVect[1]));
            return point;
        }

        public static PointF getPointFInDistance(System.Drawing.PointF start, double dist, System.Drawing.PointF end)
        {
            // get vector
            int[] vect = new int[2];
            vect[0] = Convert.ToInt32(end.X - start.X);
            vect[1] = Convert.ToInt32(end.Y - start.Y);

            double vecSize = Math.Sqrt(Math.Pow(vect[0], 2) + Math.Pow(vect[1], 2));
            double[] uVect = new double[2];
            uVect[0] = vect[0] / vecSize;
            uVect[1] = vect[1] / vecSize;

            PointF point = new System.Drawing.Point();
            point.X = (float)(start.X + (dist * uVect[0]));
            point.Y = (float)(start.Y + (dist * uVect[1]));
            return point;
        }

        public static Bitmap getVisibility(byte[] inputColorData, DepthImagePixel[] depthPixels, ColorImagePoint[] colorCoordinates, Player player, byte[] outputColorData)
        {
            byte[] colorData = new byte[inputColorData.Length];
            inputColorData.CopyTo(colorData, 0);                                 

            SystemService.getVisibilityHorizontalSegment(ref colorData, 0, Config.DepthHeight, depthPixels, colorCoordinates, player);
            SystemService.getVisibilityHorizontalSegment(ref colorData, Config.DepthWidth, Config.DepthHeight, depthPixels, colorCoordinates, player);
            SystemService.getVisibilityVerticalSegment(ref colorData, Config.DepthWidth, 0, depthPixels, colorCoordinates, player);
            SystemService.getVisibilityVerticalSegment(ref colorData, Config.DepthWidth, Config.DepthHeight, depthPixels, colorCoordinates, player);
            
            // overlay origin color data with visibility data            
            for (var i = 0; i < colorData.Length; i = i + 4)
            {
                if (colorData[i] == 0 && colorData[i + 1] == 0 && colorData[i+2] == 0 && colorData[i+3] == 0)
                {
                    outputColorData[i] = 255;
                    outputColorData[i + 1] = 255;
                    outputColorData[i + 2] = 255;
                    outputColorData[i + 3] = 0;
                }
            }          
                       
            Bitmap bmp = Converter.ByteArrayToBitmap(outputColorData, outputColorData.Length);            
            return bmp;
        }


        public static void getVisibilityVerticalSegment(ref byte[] colorData, int from, int to, DepthImagePixel[] depthPixels, ColorImagePoint[] colorCoordinates, Player player)
        {
            // player position
            System.Drawing.Point playerCenter = System.Drawing.Point.Round(player.Center);
            int playerDepthIndex = playerCenter.X + (playerCenter.Y * Config.DepthWidth);
            DepthImagePixel playerDepthPixel = depthPixels[playerDepthIndex];
            int playerDepth = (Config.SurfaceDist - playerDepthPixel.Depth) + Config.HeightOfPlayerView;
            // get color coordinates
            ColorImagePoint playerColorImagePoint = colorCoordinates[playerDepthIndex];
            int playerInDepthX = playerColorImagePoint.X;
            int playerInDepthY = playerColorImagePoint.Y;
            if (playerInDepthX > 0 && playerInDepthX < Config.DepthWidth && playerInDepthY >= 0 && playerInDepthY < Config.DepthHeight)
            {                
                int colorPos = playerInDepthX * 4 + (playerInDepthY * Config.DepthWidth * 4);
                
            }
            MCvPoint3D32f playerPoint = new MCvPoint3D32f(playerInDepthX, playerInDepthY, playerDepth);            
            
            // point position
            double maxSlope;
            DepthImagePixel pointPixel;
            int pointIndex;
            int pointDepth;
            System.Drawing.Point point;
            MCvPoint3D32f point3d;
            double slopeToPoint = 0;
            ColorImagePoint colorImagePoint;

            for (int i = 0; i < from; i++)
            {
                maxSlope = Double.MinValue;  
                // get vector from player to point on the edge                
                Vector playerToPoint = SystemService.getVector(playerCenter, new System.Drawing.Point(i, to));
                int length = (int)playerToPoint.Length;


                //border point position            
                int borderDepthIndex;
                DepthImagePixel borderDepthPixel;
                int borderDepth = 0;
                MCvPoint3D32f borderPoint = new MCvPoint3D32f(); 
                for (int tempLength = length - 1; tempLength > 0; tempLength--)
                {
                    System.Drawing.Point borderP = System.Drawing.Point.Round(SystemService.getPointInDistance(playerCenter, tempLength, new System.Drawing.Point(i, to)));
                    int borderIndex = borderP.X + (borderP.Y * Config.DepthWidth);
                    // check index
                    if (borderIndex > depthPixels.Length - 1)
                    {
                        continue;
                    }
                    // get depth pixel at this point                    
                    DepthImagePixel borderPixel = depthPixels[borderIndex];
                    // get color coordinates
                    ColorImagePoint borderColorImagePoint = colorCoordinates[borderIndex];
                    int colorInDepthX = borderColorImagePoint.X;
                    int colorInDepthY = borderColorImagePoint.Y;   
                    // check if color coordinates are valid
                    if (colorInDepthX > 0 && colorInDepthX < Config.DepthWidth && colorInDepthY >= 0 && colorInDepthY < Config.DepthHeight)
                    {
                        // get depth at this point
                        int tempDepth = borderPixel.Depth;
                        if (tempDepth != 0)
                        {
                            borderDepthIndex = borderIndex;
                            borderDepthPixel = borderPixel;
                            borderDepth = Config.SurfaceDist - tempDepth;                            
                            borderPoint = new MCvPoint3D32f(borderP.X, borderP.Y, borderDepth);
                            break;
                        }
                    }
                }

                if (borderPoint.Equals(new MCvPoint3D32f()))
                {
                    continue;
                }                              
                             
                
                // get all points on the line from player to border
                for (int tempLength = 1; tempLength < length; tempLength++)
                {
                    // point on line
                    point = System.Drawing.Point.Round(SystemService.getPointInDistance(playerCenter, tempLength, new System.Drawing.Point(i, to)));
                    pointIndex = point.X + (point.Y * Config.ColorWidth);
                    // check index
                    if (pointIndex > depthPixels.Length - 1)
                    {                        
                        continue;
                    }
                    // get depth pixel at this point                    
                    pointPixel = depthPixels[pointIndex];
                    // get color coordinates
                    colorImagePoint = colorCoordinates[pointIndex];
                    int colorInDepthX = colorImagePoint.X;
                    int colorInDepthY = colorImagePoint.Y;                    

                    // check if color coordinates are valid
                    if (colorInDepthX > 0 && colorInDepthX < Config.DepthWidth && colorInDepthY >= 0 && colorInDepthY < Config.DepthHeight)
                    {
                        // get depth at this point
                        pointDepth = pointPixel.Depth;
                        pointDepth = Config.SurfaceDist - pointDepth;
                        // 3D point at these coordinates
                        point3d = new MCvPoint3D32f(point.X, point.Y, pointDepth);    
                        slopeToPoint = SystemService.getSlope(playerPoint, point3d, borderDepth);
                        int colorPos = colorInDepthX * 4 + (colorInDepthY * Config.DepthWidth * 4);                       

                        if (slopeToPoint + (Config.VisibilityDeviation/10) < maxSlope)
                        {
                            // is hidden, 
                            colorData[colorPos] = 0;
                            colorData[colorPos + 1] = 0;
                            colorData[colorPos + 2] = 0;
                            colorData[colorPos + 3] = 0;
                        }
                        else
                        {                            
                            colorData[colorPos] = 255;
                            colorData[colorPos + 1] = 255;
                            colorData[colorPos + 2] = 255;
                            colorData[colorPos + 3] = 255;
                            
                            maxSlope = slopeToPoint;                           
                        }
                    }                    
                }
            }
        }


        public static void getVisibilityHorizontalSegment(ref byte[] colorData, int from, int to, DepthImagePixel[] depthPixels, ColorImagePoint[] colorCoordinates, Player player)
        {
            // player position
            System.Drawing.Point playerCenter = System.Drawing.Point.Round(player.Center);
            int playerDepthIndex = playerCenter.X + (playerCenter.Y * Config.DepthWidth);
            DepthImagePixel playerDepthPixel = depthPixels[playerDepthIndex];
            int playerDepth = (Config.SurfaceDist - playerDepthPixel.Depth) + Config.HeightOfPlayerView;
            // get color coordinates
            ColorImagePoint playerColorImagePoint = colorCoordinates[playerDepthIndex];
            int playerInDepthX = playerColorImagePoint.X;
            int playerInDepthY = playerColorImagePoint.Y;
            if (playerInDepthX > 0 && playerInDepthX < Config.DepthWidth && playerInDepthY >= 0 && playerInDepthY < Config.DepthHeight)
            {
                int colorPos = playerInDepthX * 4 + (playerInDepthY * Config.DepthWidth * 4);
            }
            MCvPoint3D32f playerPoint = new MCvPoint3D32f(playerInDepthX, playerInDepthY, playerDepth);

            // point position
            double maxSlope;
            DepthImagePixel pointPixel;
            int pointIndex;
            int pointDepth;
            System.Drawing.Point point;
            MCvPoint3D32f point3d;
            double slopeToPoint = 0;
            ColorImagePoint colorImagePoint;

            for (int i = 0; i < to; i++)
            {                
                maxSlope = Double.MinValue;
                // get vector from player to point on the edge                
                Vector playerToPoint = SystemService.getVector(playerCenter, new System.Drawing.Point(from, i));
                int length = (int)playerToPoint.Length;


                //border point position            
                int borderDepthIndex;
                DepthImagePixel borderDepthPixel;
                int borderDepth = 0;
                MCvPoint3D32f borderPoint = new MCvPoint3D32f();
                for (int tempLength = length - 1; tempLength > 0; tempLength--)
                {

                    System.Drawing.Point borderP = System.Drawing.Point.Round(SystemService.getPointInDistance(playerCenter, tempLength, new System.Drawing.Point(from, i)));
                    int borderIndex = borderP.X + (borderP.Y * Config.ColorWidth);                    
                    // check index
                    if (borderIndex > depthPixels.Length - 1)
                    {                       
                        continue;
                    }
                    // get depth pixel at this point                    
                    DepthImagePixel borderPixel = depthPixels[borderIndex];
                    // get color coordinates
                    ColorImagePoint borderColorImagePoint = colorCoordinates[borderIndex];
                    int colorInDepthX = borderColorImagePoint.X;
                    int colorInDepthY = borderColorImagePoint.Y;
                    // check if color coordinates are valid
                    if (colorInDepthX > 0 && colorInDepthX < Config.DepthWidth && colorInDepthY >= 0 && colorInDepthY < Config.DepthHeight)
                    {                       
                        // get depth at this point
                        int tempDepth = borderPixel.Depth;
                        if (tempDepth != 0)
                        {                            
                            borderDepthIndex = borderIndex;
                            borderDepthPixel = borderPixel;
                            borderDepth = Config.SurfaceDist - tempDepth;
                            borderPoint = new MCvPoint3D32f(borderP.X, borderP.Y, borderDepth);                           
                            break;
                        }
                    }                    
                }
                
                if (borderPoint.Equals(new MCvPoint3D32f()))
                {
                    continue;
                }

                // get all points on the line from player to border
                for (int tempLength = 1; tempLength < length; tempLength++)
                {
                    // point on line
                    point = System.Drawing.Point.Round(SystemService.getPointInDistance(playerCenter, tempLength, new System.Drawing.Point(from, i)));
                    pointIndex = point.X + (point.Y * Config.ColorWidth);
                    // check index
                    if (pointIndex > depthPixels.Length - 1)
                    {
                        continue;
                    }
                    // get depth pixel at this point                    
                    pointPixel = depthPixels[pointIndex];
                    // get color coordinates
                    colorImagePoint = colorCoordinates[pointIndex];
                    int colorInDepthX = colorImagePoint.X;
                    int colorInDepthY = colorImagePoint.Y;

                    // check if color coordinates are valid
                    if (colorInDepthX > 0 && colorInDepthX < Config.DepthWidth && colorInDepthY >= 0 && colorInDepthY < Config.DepthHeight)
                    {
                        // get depth at this point
                        pointDepth = pointPixel.Depth;
                        pointDepth = Config.SurfaceDist - pointDepth;
                        // 3D point at these coordinates
                        point3d = new MCvPoint3D32f(point.X, point.Y, pointDepth);
                        slopeToPoint = SystemService.getSlope(playerPoint, point3d, borderDepth);
                        int colorPos = colorInDepthX * 4 + (colorInDepthY * Config.DepthWidth * 4);

                        if (slopeToPoint+(Config.VisibilityDeviation/10) < maxSlope)
                        {                            
                            // is hidden
                            colorData[colorPos] = 0;
                            colorData[colorPos + 1] = 0;
                            colorData[colorPos + 2] = 0;
                            colorData[colorPos + 3] = 0;
                        }
                        else
                        {                            
                            colorData[colorPos] = 255;
                            colorData[colorPos + 1] = 255;
                            colorData[colorPos + 2] = 255;
                            colorData[colorPos + 3] = 255;
                            
                            maxSlope = slopeToPoint;                            
                        }
                    }
                }
            }
        }

        public static double getSlope(MCvPoint3D32f playerPoint, MCvPoint3D32f point, int borderDepth) 
        {            
            double numerator = point.z - playerPoint.z;
            double denominator = Math.Sqrt(Math.Pow(point.x - playerPoint.x, 2) + Math.Pow(point.y - playerPoint.y, 2));
            return numerator / denominator;
        }       

        public static Vector getVector(System.Drawing.Point p1, System.Drawing.Point p2)
        {
            int x = p2.X - p1.X;
            int y = p2.Y - p1.Y;
            return new Vector(x, y);
        }                
    }
}
