﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geodata
{
    public partial class loadMarkersForm : Form
    {
        public loadMarkersForm()
        {
            InitializeComponent();            
            this.txtTeamBlue.Text = Config.BlueMarkerPath;
            this.txtTeamGreen.Text = Config.GreenMarkerPath;
        }

        private void btnGreenTemplate_Click(object sender, EventArgs e)
        {
            string filename = "";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "PNG images|*.PNG";
            DialogResult dr = ofd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                filename = ofd.FileName;
            }
            if (filename != "")
            {
                this.txtTeamGreen.Text = filename;                        
            }
        }

        private void btnBlueTemplate_Click(object sender, EventArgs e)
        {
            string filename = "";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "PNG images|*.PNG";
            DialogResult dr = ofd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                filename = ofd.FileName;
            }
            if (filename != "")
            {
                this.txtTeamBlue.Text = filename;                
            }
        }

        private void btnLoadMarkers_Click(object sender, EventArgs e)
        {
            string greenFilename = this.txtTeamGreen.Text;
            Config.GreenMarkerPath = greenFilename;
            string blueFilename = this.txtTeamBlue.Text;
            Config.BlueMarkerPath = blueFilename;            
            this.Close();
        }
    }
}
