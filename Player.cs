﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Emgu.CV.Structure;
using Emgu.CV;

namespace Geodata
{
    class Player
    {
        public int Health = 100;                
        public MCvBox2D Body;
        public PointF Center;
        public CircleF MoveableArea;
        public double MovementDistance = 0;
        public int FrameCounter = 0;
        public bool IsActive;
        public PointF AreaCenter;
        public float direction = 0;
        public Point[] FOV;        
        public Team team;
        public Point[] Corners;
        public int removeCounter = 0;
        
        public Player(Point[] corners, Color color)
        {
            this.team = new Team(color);
            this.Corners = corners;
            this.Center = SystemService.getMidPoint(corners[0], corners[2]);
            double width = ((new LineSegment2D(corners[2], corners[3]).Length) + (new LineSegment2D(corners[0], corners[1]).Length)) / 2;
            double height = ((new LineSegment2D(corners[2], corners[1]).Length) + (new LineSegment2D(corners[0], corners[3]).Length)) / 2;            
            this.MoveableArea = new CircleF(this.Center, (float)Config.MoveableRadius);
            this.MovementDistance = Config.MoveableRadius;
            this.IsActive = false;            

            // get orientation of markers
            this.direction = (float)SystemService.getAngleBetweenLines(new LineSegment2D(new Point(0, 0), new Point(0, Config.ColorWidth)), new LineSegment2D(corners[0], corners[3]));
            if (corners[1].X > corners[2].X)
            {
                this.direction = 90.0f + (90.0f - this.direction);
                this.Body = new MCvBox2D(this.Center, new Size((int)width, (int)height), this.direction);
                List<PointF> verticesF = this.Body.GetVertices().ToList();
                Point[] vertices = verticesF.Select(p => { return Point.Round(p); }).ToArray();
                Point trPoint = SystemService.getFarPoint(vertices[2], vertices[0]);
                Point tlPoint = SystemService.getFarPoint(vertices[3], vertices[1]);
                Point trEnd = SystemService.getFarPoint(vertices[0], trPoint);
                Point tlEnd = SystemService.getFarPoint(vertices[1], tlPoint);
                this.FOV = new Point[] { vertices[1], vertices[0], trEnd, tlEnd };
            }
            else
            {
                this.Body = new MCvBox2D(this.Center, new Size((int)width, (int)height), this.direction);

                List<PointF> verticesF = this.Body.GetVertices().ToList();
                Point[] vertices = verticesF.Select(p => { return Point.Round(p); }).ToArray();
                Point trPoint = SystemService.getFarPoint(vertices[0], vertices[2]);
                Point tlPoint = SystemService.getFarPoint(vertices[1], vertices[3]);
                Point trEnd = SystemService.getFarPoint(vertices[2], trPoint);
                Point tlEnd = SystemService.getFarPoint(vertices[1], tlPoint);
                    
                this.FOV = new Point[] { vertices[3], vertices[2], trEnd, tlEnd };
            }

            this.AreaCenter = this.Body.center;                             
        }         

        public Player()
        {
            this.Health = 100;
        }

        public bool isDead()
        {
            if (this.Health <= 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get random point on the trapezium
        /// </summary>
        /// <returns></returns>
        public Point shot()
        {            
            Point trPoint = SystemService.getFarPoint(this.Corners[0], this.Corners[2]);
            Point tlPoint = SystemService.getFarPoint(this.Corners[3], this.Corners[1]);
            Point trEnd = SystemService.getFarPoint(this.Corners[2], trPoint);
            Point tlEnd = SystemService.getFarPoint(this.Corners[1], tlPoint);
            Point midPoint = SystemService.getMidPoint(trEnd, tlEnd);           

            return SystemService.getRandomPoint(midPoint);
        }
        
        /// <summary>
        /// If player is active update his position and reduce his moveable area
        /// </summary>
        /// <param name="p"></param>
        public void updateActive(Player p)
        {
            this.MoveableArea = new CircleF(this.Center, (float)this.MovementDistance);            

            double x = Math.Pow(this.Body.center.X - p.Body.center.X, 2);
            double y = Math.Pow(this.Body.center.Y - p.Body.center.Y, 2);
            double dist = Math.Sqrt(x + y);
           
            if ((this.MovementDistance - dist) > 0.0)
            {
                Console.WriteLine("dist " + dist.ToString());
                this.Body = p.Body;
                this.Center = p.Body.center;
                this.direction = p.direction;
                this.FOV = p.FOV;
                this.Corners = p.Corners;
                if (dist > 10)
                {
                    this.MovementDistance -= dist;
                }
            }
        }

        /// <summary>
        /// Update player position and orietation
        /// </summary>
        /// <param name="p"></param>
        public void update(Player p) 
        {            
            this.direction = p.direction;            
            this.MoveableArea.Center = p.Center;
            this.Body = p.Body;
            this.Center = p.Body.center;
            this.direction = p.direction;
            this.FOV = p.FOV;
            this.Corners = p.Corners;              
        }

        public void updateOrientation(Player p)
        {            
            if(hasSameCenter(p.Center)) {               
                this.Body = p.Body;
                this.Center = p.Body.center;
                this.direction = p.direction;
                this.FOV = p.FOV;
                this.Corners = p.Corners; 
            }
        }
        
        /// <summary>
        /// Detect if player has same center as another player
        /// </summary>
        /// <param name="center">Center of other player</param>
        /// <returns></returns>
        public bool hasSameCenter(PointF center)
        {
            int elevation = 1;
            if ((center.X < Center.X + elevation) || (center.X > Center.X - elevation))
            {
                if ((center.Y < Center.Y + elevation) || (center.Y > Center.Y - elevation))
                {
                    return true;
                }
            }
            return false;
        }

        public override bool Equals(object obj)
        {
            Player other = obj as Player;
            if (other == null)
                return false;
            else
            {
                double radius = SystemService.getDistBetweenTwoPoints(this.Center, other.Center);
                         
                if (radius < Config.IsEqualsRadius)
                {                    
                    return true;
                }
                else
                {
                    return false;
                }                
            }            
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }        
    }
}
