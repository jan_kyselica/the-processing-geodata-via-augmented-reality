﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Media;
using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace Geodata
{
    class Detector
    {
        public Object DetectedObject { get; set; }
        public Player Player { get; set; }
        public bool Detected { get; set; }
        public List<MCvBox2D> obstacle { get; set; }

        public Detector()
        {
            this.DetectedObject = null;
        }

        public List<Player> SURFFindTemplate(Image<Bgr, Byte> img, Image<Bgr, Byte> template, System.Drawing.Color color)
        {
            Image<Gray, Byte> modelImage = template.Convert<Gray, Byte>();
            Image<Gray, Byte> observedImage = img.Convert<Gray, Byte>();

            HomographyMatrix homography = null;

            SURFDetector surfCPU = new SURFDetector(500, false);
            VectorOfKeyPoint modelKeyPoints;
            VectorOfKeyPoint observedKeyPoints;
            Matrix<int> indices;

            Matrix<byte> mask;
            int k = 2;
            double uniquenessThreshold = 0.8;

            //extract features from the object image
            modelKeyPoints = surfCPU.DetectKeyPointsRaw(modelImage, null);
            Matrix<float> modelDescriptors = surfCPU.ComputeDescriptorsRaw(modelImage, null, modelKeyPoints);

            // extract features from the observed image
            observedKeyPoints = surfCPU.DetectKeyPointsRaw(observedImage, null);
            Matrix<float> observedDescriptors = surfCPU.ComputeDescriptorsRaw(observedImage, null, observedKeyPoints);

            BruteForceMatcher<float> matcher = new BruteForceMatcher<float>(DistanceType.L2);
            matcher.Add(modelDescriptors);

            indices = new Matrix<int>(observedDescriptors.Rows, k);
            using (Matrix<float> dist = new Matrix<float>(observedDescriptors.Rows, k))
            {
                matcher.KnnMatch(observedDescriptors, indices, dist, k, null);
                mask = new Matrix<byte>(dist.Rows, 1);
                mask.SetValue(255);
                Features2DToolbox.VoteForUniqueness(dist, uniquenessThreshold, mask);
            }

            int nonZeroCount = CvInvoke.cvCountNonZero(mask);
            if (nonZeroCount >= 4)
            {
                nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints, indices, mask, 1.5, 20);
                if (nonZeroCount >= 4)
                    homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints, observedKeyPoints, indices, mask, 2);
            }

            List<Player> players = new List<Player>();
            Image<Bgr, Byte> result = img;

            #region draw the projected region on the image
            if (homography != null)
            {  //draw a rectangle along the projected model
                Rectangle rect = modelImage.ROI;
                PointF[] pts = new PointF[] { 
                       new PointF(rect.Left, rect.Bottom),
                       new PointF(rect.Right, rect.Bottom),
                       new PointF(rect.Right, rect.Top),
                       new PointF(rect.Left, rect.Top)};

                homography.ProjectPoints(pts);
                Point[] ps = Array.ConvertAll<PointF, Point>(pts, Point.Round);
                bool isBox = SystemService.isBox(ps);

                if (isBox)
                {
                    result.DrawPolyline(ps, true, new Bgr(System.Drawing.Color.Brown), 4);
                    result.Draw(new LineSegment2D(ps[0], ps[3]), new Bgr(System.Drawing.Color.DarkCyan), 6);
                    result.Draw(new LineSegment2D(ps[0], ps[1]), new Bgr(System.Drawing.Color.Orange), 6);
                    isBox = false;
                    Player p = new Player(ps, color);
                    float size = p.Body.size.Width * p.Body.size.Width;
                    if (size > Config.MinPlayerArea && size < Config.MaxPlayerArea)
                    {
                        players.Add(p);
                    }
                }
            }
            #endregion

            return players;
        }

        // used for detecting obstacles
        public Contour<Point> findObstacles(Image<Bgr, Byte> img)
        {
            Image<Gray, byte> gray_image = img.Convert<Gray, byte>();
            gray_image = gray_image.ThresholdBinary(new Gray(Config.HeightOfObstacles), new Gray(255));            
            this.obstacle = new List<MCvBox2D>();
            Contour<Point> contours, res;
            using (MemStorage stor = new MemStorage())
            {
                contours = gray_image.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL, stor);
                res = contours;

                for (int i = 0; contours != null; contours = contours.HNext)
                {
                    i++;
                    if ((contours.Area < Config.MaxObstaclesArea) && (contours.Area > Config.MinObstaclesArea))
                    {
                        MCvBox2D box = contours.GetMinAreaRect();

                        obstacle.Add(box);
                    }
                }
            }

            return res;
        }
    }
}
