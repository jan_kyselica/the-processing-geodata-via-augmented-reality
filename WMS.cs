﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using BitMiracle.LibTiff.Classic;
using System.Windows.Forms;

namespace Geodata
{
    class WMS : IO
    {
        public static string TiffUrl = null;
        public static string PngUrl = null;

        public string Url { get; set; }

        public override void loadTiff()
        {            
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(WMS.TiffUrl);
            httpWebRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
            try
            {
                using (HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    if (httpWebResponse.StatusCode == HttpStatusCode.OK && httpWebResponse.ContentType == "image/tiff")
                    {
                        MemoryStream ms = new MemoryStream();
                        using (Stream stream = httpWebResponse.GetResponseStream())
                        {
                            stream.CopyTo(ms);
                            Image img = Image.FromStream(ms);
                            ms.Position = 0;
                            image = Tiff.ClientOpen("in-memory", "r", ms, new TiffStream());
                        }
                        this.readRGB();
                    }
                    else
                    {
                        MessageBox.Show(getError(httpWebResponse, "TIFF"));
                    }
                }
            }
            catch (WebException e)
            {
                MessageBox.Show("Connecton error" + Environment.NewLine + e.Message);

            }
        }

        public override void loadPng()
        {
            try 
            {
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(WMS.PngUrl);
            httpWebRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
            using (HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                if (httpWebResponse.StatusCode == HttpStatusCode.OK && httpWebResponse.ContentType == "image/png")
                {
                    MemoryStream ms = new MemoryStream();
                    using (Stream stream = httpWebResponse.GetResponseStream())
                    {
                        stream.CopyTo(ms);
                        Image img = Image.FromStream(ms);
                        this.Map = new Bitmap(img);

                        // validate resolution of map
                        if (!this.validateMap())
                        {
                            MessageBox.Show("Resolution of image must be 480 x 640 px");
                            return;
                        }
                    }                    
                }
                else 
                {
                    MessageBox.Show(getError(httpWebResponse, "PNG"));
                }                
            }
            }
            catch (WebException e)
            {
                MessageBox.Show("Connecton error" + Environment.NewLine + e.Message);

            }
        }

        /// <summary>
        /// Propagate error from http response to message box
        /// </summary>
        /// <param name="httpWebResponse"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private String getError(HttpWebResponse httpWebResponse, String type)
        {
            String text = "Bad GET request" + Environment.NewLine + Environment.NewLine;
            if (httpWebResponse.ContentType == "text/xml")
            {
                using (Stream stream = httpWebResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        String response = reader.ReadToEnd();
                        try
                        {
                            XDocument xdoc = XDocument.Parse(response);
                            Console.WriteLine(xdoc);
                            text += xdoc.ToString();
                            Console.WriteLine(text);
                        }
                        catch (XmlException e)
                        {
                            text = "Internal error" + Environment.NewLine + Environment.NewLine;
                            text += e.Message;
                        }

                    }
                }
            }
            else
            {
                if (type == "PNG")
                {
                    return "The type of PNG Image has to be image/png";
                }
                else
                {
                    return "The type of TIFF Image has to be image/tiff";
                }
            }
            return text;
        }        
    }
}
