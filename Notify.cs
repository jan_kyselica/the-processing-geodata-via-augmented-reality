﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Drawing;

namespace Geodata
{
    /// <summary>
    /// Class for showing messages on the background
    /// </summary>
    static class Notify
    {
        public static bool show { get; set; }
        public static string message { get; set; }
        public const int delay = 25;
        public static float scale { get; set; }
        public static MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 1.5, 1.5);

        public static Image<Bgr, Byte> showMessage(Image<Bgr, Byte> img)
        {
            font.hscale = Notify.scale;
            font.vscale = Notify.scale;
            Point p = new Point(20, 50);
            img.Draw(Notify.message, ref font, p, new Bgr(Color.Tomato));
            return img;
        }

        public static Image<Bgr, Byte> endGame(Image<Bgr, Byte> img)
        {
            font.hscale = Notify.scale;
            font.vscale = Notify.scale;
            Point p = new Point(img.Width/2 - 50, img.Height/2);
            img.Draw(Notify.message, ref font, p, new Bgr(Color.Tomato));
            return img;
        }
    }
}
