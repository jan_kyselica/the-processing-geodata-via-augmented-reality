﻿using System;
using System.Windows.Forms;


namespace Geodata
{
    class Apps
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Output());
        }
    }
}