﻿namespace Geodata
{
    partial class Output
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Output));
            this.imgOutput = new Emgu.CV.UI.ImageBox();
            this.lblSurface = new System.Windows.Forms.Label();
            this.modelHeight = new System.Windows.Forms.TrackBar();
            this.maxObstacleArea = new System.Windows.Forms.TrackBar();
            this.minObstacleArea = new System.Windows.Forms.TrackBar();
            this.lblObstacleMin = new System.Windows.Forms.Label();
            this.lblObstacleMax = new System.Windows.Forms.Label();
            this.groupPlayer = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.visibilityDeviation = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.sizeOfShot = new System.Windows.Forms.TrackBar();
            this.shotDispersion = new System.Windows.Forms.TrackBar();
            this.minPlayerArea = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.maxPlayerArea = new System.Windows.Forms.TrackBar();
            this.moveableRadius = new System.Windows.Forms.TrackBar();
            this.lblMinPlayerArea = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblMaxPlayerArea = new System.Windows.Forms.Label();
            this.playerView = new System.Windows.Forms.TrackBar();
            this.lblPlayerView = new System.Windows.Forms.Label();
            this.groupSurface = new System.Windows.Forms.GroupBox();
            this.heightOfObstacle = new System.Windows.Forms.TrackBar();
            this.label12 = new System.Windows.Forms.Label();
            this.imgWMS = new Emgu.CV.UI.ImageBox();
            this.imgDepth = new Emgu.CV.UI.ImageBox();
            this.imgMap = new Emgu.CV.UI.ImageBox();
            this.txtFps = new System.Windows.Forms.Label();
            this.boxLowColor = new System.Windows.Forms.PictureBox();
            this.boxHighColor = new System.Windows.Forms.PictureBox();
            this.lblLowColor = new System.Windows.Forms.Label();
            this.lblHighColor = new System.Windows.Forms.Label();
            this.menu = new System.Windows.Forms.ToolStrip();
            this.btnLoad = new System.Windows.Forms.ToolStripButton();
            this.btnModel = new System.Windows.Forms.ToolStripButton();
            this.btnPlay = new System.Windows.Forms.ToolStripButton();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.btnSaveAsPng = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveAsTiff = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSettings = new System.Windows.Forms.ToolStripSplitButton();
            this.btnDetectPlayers = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCompareHeights = new System.Windows.Forms.ToolStripMenuItem();
            this.bthHighlightObstacles = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLoadMarkers = new System.Windows.Forms.ToolStripMenuItem();
            this.btnReset = new System.Windows.Forms.ToolStripButton();
            this.btnExit = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupGame = new System.Windows.Forms.GroupBox();
            this.numbersOfPlayers = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupModeling = new System.Windows.Forms.GroupBox();
            this.deviation = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.imgColor = new Emgu.CV.UI.ImageBox();
            this.label6 = new System.Windows.Forms.Label();
            this.imgModeling = new Emgu.CV.UI.ImageBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxObstacleArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minObstacleArea)).BeginInit();
            this.groupPlayer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visibilityDeviation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOfShot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shotDispersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minPlayerArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxPlayerArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveableRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerView)).BeginInit();
            this.groupSurface.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heightOfObstacle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgWMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDepth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxLowColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxHighColor)).BeginInit();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupGame.SuspendLayout();
            this.groupModeling.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deviation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgModeling)).BeginInit();
            this.SuspendLayout();
            // 
            // imgOutput
            // 
            this.imgOutput.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.imgOutput.Location = new System.Drawing.Point(378, 601);
            this.imgOutput.Margin = new System.Windows.Forms.Padding(15);
            this.imgOutput.Name = "imgOutput";
            this.imgOutput.Size = new System.Drawing.Size(320, 240);
            this.imgOutput.TabIndex = 2;
            this.imgOutput.TabStop = false;
            // 
            // lblSurface
            // 
            this.lblSurface.AutoSize = true;
            this.lblSurface.Location = new System.Drawing.Point(6, 138);
            this.lblSurface.Name = "lblSurface";
            this.lblSurface.Size = new System.Drawing.Size(81, 13);
            this.lblSurface.TabIndex = 3;
            this.lblSurface.Text = "Height of model";
            // 
            // modelHeight
            // 
            this.modelHeight.LargeChange = 40;
            this.modelHeight.Location = new System.Drawing.Point(166, 138);
            this.modelHeight.Maximum = 80;
            this.modelHeight.Name = "modelHeight";
            this.modelHeight.Size = new System.Drawing.Size(229, 45);
            this.modelHeight.SmallChange = 2;
            this.modelHeight.TabIndex = 8;
            this.modelHeight.TickFrequency = 4;
            this.modelHeight.Value = 5;
            this.modelHeight.Scroll += new System.EventHandler(this.modelHeight_Scroll);
            this.modelHeight.ValueChanged += new System.EventHandler(this.setSurfaceDistance);
            // 
            // maxObstacleArea
            // 
            this.maxObstacleArea.LargeChange = 1000;
            this.maxObstacleArea.Location = new System.Drawing.Point(164, 19);
            this.maxObstacleArea.Maximum = 10000;
            this.maxObstacleArea.Minimum = 2000;
            this.maxObstacleArea.Name = "maxObstacleArea";
            this.maxObstacleArea.RightToLeftLayout = true;
            this.maxObstacleArea.Size = new System.Drawing.Size(229, 45);
            this.maxObstacleArea.SmallChange = 500;
            this.maxObstacleArea.TabIndex = 9;
            this.maxObstacleArea.TickFrequency = 750;
            this.maxObstacleArea.Value = 8000;
            this.maxObstacleArea.ValueChanged += new System.EventHandler(this.setMaxObstacleArea);
            // 
            // minObstacleArea
            // 
            this.minObstacleArea.LargeChange = 1000;
            this.minObstacleArea.Location = new System.Drawing.Point(166, 70);
            this.minObstacleArea.Maximum = 5000;
            this.minObstacleArea.Name = "minObstacleArea";
            this.minObstacleArea.Size = new System.Drawing.Size(229, 45);
            this.minObstacleArea.SmallChange = 500;
            this.minObstacleArea.TabIndex = 10;
            this.minObstacleArea.TickFrequency = 500;
            this.minObstacleArea.Value = 1000;
            this.minObstacleArea.ValueChanged += new System.EventHandler(this.setMinObstacleArea);
            // 
            // lblObstacleMin
            // 
            this.lblObstacleMin.AutoSize = true;
            this.lblObstacleMin.Location = new System.Drawing.Point(4, 70);
            this.lblObstacleMin.Name = "lblObstacleMin";
            this.lblObstacleMin.Size = new System.Drawing.Size(103, 13);
            this.lblObstacleMin.TabIndex = 10;
            this.lblObstacleMin.Text = "Min area of obstacle";
            // 
            // lblObstacleMax
            // 
            this.lblObstacleMax.AutoSize = true;
            this.lblObstacleMax.Location = new System.Drawing.Point(4, 19);
            this.lblObstacleMax.Name = "lblObstacleMax";
            this.lblObstacleMax.Size = new System.Drawing.Size(106, 13);
            this.lblObstacleMax.TabIndex = 9;
            this.lblObstacleMax.Text = "Max area of obstacle";
            // 
            // groupPlayer
            // 
            this.groupPlayer.Controls.Add(this.label13);
            this.groupPlayer.Controls.Add(this.visibilityDeviation);
            this.groupPlayer.Controls.Add(this.label11);
            this.groupPlayer.Controls.Add(this.sizeOfShot);
            this.groupPlayer.Controls.Add(this.shotDispersion);
            this.groupPlayer.Controls.Add(this.minPlayerArea);
            this.groupPlayer.Controls.Add(this.label10);
            this.groupPlayer.Controls.Add(this.maxPlayerArea);
            this.groupPlayer.Controls.Add(this.moveableRadius);
            this.groupPlayer.Controls.Add(this.lblMinPlayerArea);
            this.groupPlayer.Controls.Add(this.label9);
            this.groupPlayer.Controls.Add(this.lblMaxPlayerArea);
            this.groupPlayer.Controls.Add(this.playerView);
            this.groupPlayer.Controls.Add(this.lblPlayerView);
            this.groupPlayer.Location = new System.Drawing.Point(15, 106);
            this.groupPlayer.Name = "groupPlayer";
            this.groupPlayer.Size = new System.Drawing.Size(413, 391);
            this.groupPlayer.TabIndex = 10;
            this.groupPlayer.TabStop = false;
            this.groupPlayer.Text = "Player";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 65);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Visibility deviation";
            // 
            // visibilityDeviation
            // 
            this.visibilityDeviation.LargeChange = 2;
            this.visibilityDeviation.Location = new System.Drawing.Point(192, 65);
            this.visibilityDeviation.Maximum = 50;
            this.visibilityDeviation.Name = "visibilityDeviation";
            this.visibilityDeviation.Size = new System.Drawing.Size(203, 45);
            this.visibilityDeviation.TabIndex = 2;
            this.visibilityDeviation.TickFrequency = 2;
            this.visibilityDeviation.Value = 2;
            this.visibilityDeviation.Scroll += new System.EventHandler(this.visibilityDeviation_Scroll);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 341);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Size of shot";
            // 
            // sizeOfShot
            // 
            this.sizeOfShot.LargeChange = 6;
            this.sizeOfShot.Location = new System.Drawing.Point(190, 341);
            this.sizeOfShot.Maximum = 24;
            this.sizeOfShot.Minimum = 2;
            this.sizeOfShot.Name = "sizeOfShot";
            this.sizeOfShot.Size = new System.Drawing.Size(203, 45);
            this.sizeOfShot.SmallChange = 2;
            this.sizeOfShot.TabIndex = 7;
            this.sizeOfShot.TickFrequency = 2;
            this.sizeOfShot.Value = 10;
            this.sizeOfShot.Scroll += new System.EventHandler(this.sizeOfShot_Scroll_1);
            // 
            // shotDispersion
            // 
            this.shotDispersion.LargeChange = 6;
            this.shotDispersion.Location = new System.Drawing.Point(190, 283);
            this.shotDispersion.Maximum = 24;
            this.shotDispersion.Minimum = 2;
            this.shotDispersion.Name = "shotDispersion";
            this.shotDispersion.Size = new System.Drawing.Size(203, 45);
            this.shotDispersion.SmallChange = 2;
            this.shotDispersion.TabIndex = 6;
            this.shotDispersion.TickFrequency = 2;
            this.shotDispersion.Value = 6;
            this.shotDispersion.Scroll += new System.EventHandler(this.sizeOfShot_Scroll);
            // 
            // minPlayerArea
            // 
            this.minPlayerArea.LargeChange = 1000;
            this.minPlayerArea.Location = new System.Drawing.Point(190, 168);
            this.minPlayerArea.Maximum = 2000;
            this.minPlayerArea.Name = "minPlayerArea";
            this.minPlayerArea.Size = new System.Drawing.Size(203, 45);
            this.minPlayerArea.SmallChange = 250;
            this.minPlayerArea.TabIndex = 4;
            this.minPlayerArea.TickFrequency = 250;
            this.minPlayerArea.Value = 500;
            this.minPlayerArea.ValueChanged += new System.EventHandler(this.setMinPlayerArea);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 283);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Shot dispersion";
            // 
            // maxPlayerArea
            // 
            this.maxPlayerArea.LargeChange = 1000;
            this.maxPlayerArea.Location = new System.Drawing.Point(190, 116);
            this.maxPlayerArea.Maximum = 20000;
            this.maxPlayerArea.Minimum = 2000;
            this.maxPlayerArea.Name = "maxPlayerArea";
            this.maxPlayerArea.Size = new System.Drawing.Size(203, 45);
            this.maxPlayerArea.SmallChange = 500;
            this.maxPlayerArea.TabIndex = 3;
            this.maxPlayerArea.TickFrequency = 1000;
            this.maxPlayerArea.Value = 15000;
            this.maxPlayerArea.ValueChanged += new System.EventHandler(this.setMaxPlayerArea);
            // 
            // moveableRadius
            // 
            this.moveableRadius.LargeChange = 50;
            this.moveableRadius.Location = new System.Drawing.Point(190, 221);
            this.moveableRadius.Maximum = 250;
            this.moveableRadius.Minimum = 50;
            this.moveableRadius.Name = "moveableRadius";
            this.moveableRadius.Size = new System.Drawing.Size(203, 45);
            this.moveableRadius.SmallChange = 10;
            this.moveableRadius.TabIndex = 5;
            this.moveableRadius.TickFrequency = 20;
            this.moveableRadius.Value = 150;
            this.moveableRadius.ValueChanged += new System.EventHandler(this.moveableRadius_ValueChanged);
            // 
            // lblMinPlayerArea
            // 
            this.lblMinPlayerArea.AutoSize = true;
            this.lblMinPlayerArea.Location = new System.Drawing.Point(4, 168);
            this.lblMinPlayerArea.Name = "lblMinPlayerArea";
            this.lblMinPlayerArea.Size = new System.Drawing.Size(95, 13);
            this.lblMinPlayerArea.TabIndex = 3;
            this.lblMinPlayerArea.Text = "Min area of marker";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 221);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Moveable radius";
            // 
            // lblMaxPlayerArea
            // 
            this.lblMaxPlayerArea.AutoSize = true;
            this.lblMaxPlayerArea.Location = new System.Drawing.Point(4, 116);
            this.lblMaxPlayerArea.Name = "lblMaxPlayerArea";
            this.lblMaxPlayerArea.Size = new System.Drawing.Size(98, 13);
            this.lblMaxPlayerArea.TabIndex = 2;
            this.lblMaxPlayerArea.Text = "Max area of marker";
            // 
            // playerView
            // 
            this.playerView.LargeChange = 100;
            this.playerView.Location = new System.Drawing.Point(192, 20);
            this.playerView.Maximum = 500;
            this.playerView.Name = "playerView";
            this.playerView.Size = new System.Drawing.Size(203, 45);
            this.playerView.SmallChange = 50;
            this.playerView.TabIndex = 2;
            this.playerView.TickFrequency = 50;
            this.playerView.Value = 100;
            this.playerView.ValueChanged += new System.EventHandler(this.setHeightOfPlayerView);
            // 
            // lblPlayerView
            // 
            this.lblPlayerView.AutoSize = true;
            this.lblPlayerView.Location = new System.Drawing.Point(6, 20);
            this.lblPlayerView.Name = "lblPlayerView";
            this.lblPlayerView.Size = new System.Drawing.Size(68, 13);
            this.lblPlayerView.TabIndex = 0;
            this.lblPlayerView.Text = "Player height";
            // 
            // groupSurface
            // 
            this.groupSurface.Controls.Add(this.heightOfObstacle);
            this.groupSurface.Controls.Add(this.label12);
            this.groupSurface.Controls.Add(this.lblObstacleMax);
            this.groupSurface.Controls.Add(this.lblObstacleMin);
            this.groupSurface.Controls.Add(this.minObstacleArea);
            this.groupSurface.Controls.Add(this.maxObstacleArea);
            this.groupSurface.Location = new System.Drawing.Point(15, 503);
            this.groupSurface.Name = "groupSurface";
            this.groupSurface.Size = new System.Drawing.Size(413, 168);
            this.groupSurface.TabIndex = 11;
            this.groupSurface.TabStop = false;
            this.groupSurface.Text = "Model";
            // 
            // heightOfObstacle
            // 
            this.heightOfObstacle.LargeChange = 50;
            this.heightOfObstacle.Location = new System.Drawing.Point(164, 121);
            this.heightOfObstacle.Maximum = 255;
            this.heightOfObstacle.Minimum = 1;
            this.heightOfObstacle.Name = "heightOfObstacle";
            this.heightOfObstacle.Size = new System.Drawing.Size(229, 45);
            this.heightOfObstacle.SmallChange = 50;
            this.heightOfObstacle.TabIndex = 11;
            this.heightOfObstacle.TickFrequency = 10;
            this.heightOfObstacle.Value = 170;
            this.heightOfObstacle.Scroll += new System.EventHandler(this.heightOfObstacle_Scroll);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 121);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Height of obstacles";
            // 
            // imgWMS
            // 
            this.imgWMS.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.imgWMS.Location = new System.Drawing.Point(378, 318);
            this.imgWMS.Margin = new System.Windows.Forms.Padding(15);
            this.imgWMS.Name = "imgWMS";
            this.imgWMS.Size = new System.Drawing.Size(320, 240);
            this.imgWMS.TabIndex = 15;
            this.imgWMS.TabStop = false;
            // 
            // imgDepth
            // 
            this.imgDepth.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.imgDepth.Location = new System.Drawing.Point(381, 32);
            this.imgDepth.Margin = new System.Windows.Forms.Padding(15);
            this.imgDepth.Name = "imgDepth";
            this.imgDepth.Size = new System.Drawing.Size(320, 240);
            this.imgDepth.TabIndex = 16;
            this.imgDepth.TabStop = false;
            // 
            // imgMap
            // 
            this.imgMap.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.imgMap.Location = new System.Drawing.Point(16, 318);
            this.imgMap.Margin = new System.Windows.Forms.Padding(15);
            this.imgMap.Name = "imgMap";
            this.imgMap.Size = new System.Drawing.Size(320, 240);
            this.imgMap.TabIndex = 17;
            this.imgMap.TabStop = false;
            // 
            // txtFps
            // 
            this.txtFps.AutoSize = true;
            this.txtFps.Location = new System.Drawing.Point(642, 860);
            this.txtFps.Name = "txtFps";
            this.txtFps.Size = new System.Drawing.Size(30, 13);
            this.txtFps.TabIndex = 18;
            this.txtFps.Text = "FPS:";
            // 
            // boxLowColor
            // 
            this.boxLowColor.Location = new System.Drawing.Point(12, 40);
            this.boxLowColor.Name = "boxLowColor";
            this.boxLowColor.Size = new System.Drawing.Size(99, 30);
            this.boxLowColor.TabIndex = 20;
            this.boxLowColor.TabStop = false;
            this.boxLowColor.Click += new System.EventHandler(this.selectColor);
            // 
            // boxHighColor
            // 
            this.boxHighColor.Location = new System.Drawing.Point(230, 40);
            this.boxHighColor.Name = "boxHighColor";
            this.boxHighColor.Size = new System.Drawing.Size(99, 30);
            this.boxHighColor.TabIndex = 21;
            this.boxHighColor.TabStop = false;
            this.boxHighColor.Click += new System.EventHandler(this.selectColor);
            // 
            // lblLowColor
            // 
            this.lblLowColor.AutoSize = true;
            this.lblLowColor.Location = new System.Drawing.Point(9, 24);
            this.lblLowColor.Name = "lblLowColor";
            this.lblLowColor.Size = new System.Drawing.Size(145, 13);
            this.lblLowColor.TabIndex = 22;
            this.lblLowColor.Text = "Color of area which is too low";
            // 
            // lblHighColor
            // 
            this.lblHighColor.AutoSize = true;
            this.lblHighColor.Location = new System.Drawing.Point(227, 24);
            this.lblHighColor.Name = "lblHighColor";
            this.lblHighColor.Size = new System.Drawing.Size(149, 13);
            this.lblHighColor.TabIndex = 23;
            this.lblHighColor.Text = "Color of area which is too high";
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnLoad,
            this.btnModel,
            this.btnPlay,
            this.toolStripSplitButton1,
            this.btnSettings,
            this.btnReset,
            this.btnExit});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menu.Size = new System.Drawing.Size(1184, 58);
            this.menu.TabIndex = 24;
            this.menu.Text = "Menu bar";
            // 
            // btnLoad
            // 
            this.btnLoad.Image = ((System.Drawing.Image)(resources.GetObject("btnLoad.Image")));
            this.btnLoad.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLoad.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnLoad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLoad.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(63, 51);
            this.btnLoad.Text = "Load data";
            this.btnLoad.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnLoad.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnLoad.ToolTipText = "Load data";
            this.btnLoad.Click += new System.EventHandler(this.loadData_click);
            // 
            // btnModel
            // 
            this.btnModel.Enabled = false;
            this.btnModel.Image = ((System.Drawing.Image)(resources.GetObject("btnModel.Image")));
            this.btnModel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnModel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnModel.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.btnModel.Name = "btnModel";
            this.btnModel.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnModel.Size = new System.Drawing.Size(65, 51);
            this.btnModel.Text = "Model";
            this.btnModel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnModel.Click += new System.EventHandler(this.showFullscreen);
            // 
            // btnPlay
            // 
            this.btnPlay.Image = ((System.Drawing.Image)(resources.GetObject("btnPlay.Image")));
            this.btnPlay.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPlay.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnPlay.Size = new System.Drawing.Size(56, 51);
            this.btnPlay.Text = "Play";
            this.btnPlay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPlay.Click += new System.EventHandler(this.showFullscreen);
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSaveAsPng,
            this.btnSaveAsTiff});
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.toolStripSplitButton1.Size = new System.Drawing.Size(76, 51);
            this.toolStripSplitButton1.Text = "Export";
            this.toolStripSplitButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnSaveAsPng
            // 
            this.btnSaveAsPng.Name = "btnSaveAsPng";
            this.btnSaveAsPng.Size = new System.Drawing.Size(139, 22);
            this.btnSaveAsPng.Text = "Save as PNG";
            this.btnSaveAsPng.Click += new System.EventHandler(this.saveAsPng);
            // 
            // btnSaveAsTiff
            // 
            this.btnSaveAsTiff.Name = "btnSaveAsTiff";
            this.btnSaveAsTiff.Size = new System.Drawing.Size(139, 22);
            this.btnSaveAsTiff.Text = "Save as TIFF";
            this.btnSaveAsTiff.Click += new System.EventHandler(this.saveAsTiff);
            // 
            // btnSettings
            // 
            this.btnSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDetectPlayers,
            this.btnCompareHeights,
            this.bthHighlightObstacles,
            this.btnLoadMarkers});
            this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
            this.btnSettings.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSettings.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSettings.Size = new System.Drawing.Size(85, 51);
            this.btnSettings.Text = "Settings";
            this.btnSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnDetectPlayers
            // 
            this.btnDetectPlayers.Checked = true;
            this.btnDetectPlayers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnDetectPlayers.Name = "btnDetectPlayers";
            this.btnDetectPlayers.Size = new System.Drawing.Size(176, 22);
            this.btnDetectPlayers.Text = "Detect players";
            this.btnDetectPlayers.Click += new System.EventHandler(this.btnDetectPlayers_Click);
            // 
            // btnCompareHeights
            // 
            this.btnCompareHeights.Checked = true;
            this.btnCompareHeights.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnCompareHeights.Enabled = false;
            this.btnCompareHeights.Name = "btnCompareHeights";
            this.btnCompareHeights.Size = new System.Drawing.Size(176, 22);
            this.btnCompareHeights.Text = "Compare heights";
            this.btnCompareHeights.Click += new System.EventHandler(this.btnCompareHeights_Click);
            // 
            // bthHighlightObstacles
            // 
            this.bthHighlightObstacles.Name = "bthHighlightObstacles";
            this.bthHighlightObstacles.Size = new System.Drawing.Size(176, 22);
            this.bthHighlightObstacles.Text = "Highlight obstacles";
            this.bthHighlightObstacles.Click += new System.EventHandler(this.bthHighlightObstacles_Click);
            // 
            // btnLoadMarkers
            // 
            this.btnLoadMarkers.Name = "btnLoadMarkers";
            this.btnLoadMarkers.Size = new System.Drawing.Size(176, 22);
            this.btnLoadMarkers.Text = "Load templates";
            this.btnLoadMarkers.Click += new System.EventHandler(this.btnLoadMarkers_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.Image")));
            this.btnReset.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnReset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReset.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnReset.Size = new System.Drawing.Size(67, 51);
            this.btnReset.Text = "Restart";
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnExit
            // 
            this.btnExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExit.Margin = new System.Windows.Forms.Padding(0, 5, 0, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnExit.Size = new System.Drawing.Size(56, 51);
            this.btnExit.Text = "Exit";
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(9, 60);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupGame);
            this.splitContainer1.Panel1.Controls.Add(this.groupModeling);
            this.splitContainer1.Panel1.Controls.Add(this.groupPlayer);
            this.splitContainer1.Panel1.Controls.Add(this.groupSurface);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label7);
            this.splitContainer1.Panel2.Controls.Add(this.imgColor);
            this.splitContainer1.Panel2.Controls.Add(this.label6);
            this.splitContainer1.Panel2.Controls.Add(this.imgModeling);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.imgMap);
            this.splitContainer1.Panel2.Controls.Add(this.imgDepth);
            this.splitContainer1.Panel2.Controls.Add(this.imgWMS);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.imgOutput);
            this.splitContainer1.Panel2.Controls.Add(this.txtFps);
            this.splitContainer1.Size = new System.Drawing.Size(1163, 890);
            this.splitContainer1.SplitterDistance = 446;
            this.splitContainer1.TabIndex = 25;
            // 
            // groupGame
            // 
            this.groupGame.Controls.Add(this.numbersOfPlayers);
            this.groupGame.Controls.Add(this.label8);
            this.groupGame.Location = new System.Drawing.Point(15, 17);
            this.groupGame.Name = "groupGame";
            this.groupGame.Size = new System.Drawing.Size(413, 69);
            this.groupGame.TabIndex = 25;
            this.groupGame.TabStop = false;
            this.groupGame.Text = "Game";
            // 
            // numbersOfPlayers
            // 
            this.numbersOfPlayers.FormattingEnabled = true;
            this.numbersOfPlayers.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.numbersOfPlayers.Location = new System.Drawing.Point(192, 25);
            this.numbersOfPlayers.Name = "numbersOfPlayers";
            this.numbersOfPlayers.Size = new System.Drawing.Size(203, 21);
            this.numbersOfPlayers.TabIndex = 1;
            this.numbersOfPlayers.Text = "2";
            this.numbersOfPlayers.SelectedIndexChanged += new System.EventHandler(this.changeNumberOfPlayers);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Number of players";
            // 
            // groupModeling
            // 
            this.groupModeling.Controls.Add(this.deviation);
            this.groupModeling.Controls.Add(this.modelHeight);
            this.groupModeling.Controls.Add(this.lblSurface);
            this.groupModeling.Controls.Add(this.label5);
            this.groupModeling.Controls.Add(this.lblLowColor);
            this.groupModeling.Controls.Add(this.lblHighColor);
            this.groupModeling.Controls.Add(this.boxLowColor);
            this.groupModeling.Controls.Add(this.boxHighColor);
            this.groupModeling.Enabled = false;
            this.groupModeling.Location = new System.Drawing.Point(15, 677);
            this.groupModeling.Name = "groupModeling";
            this.groupModeling.Size = new System.Drawing.Size(413, 196);
            this.groupModeling.TabIndex = 24;
            this.groupModeling.TabStop = false;
            this.groupModeling.Text = "Modeling options";
            // 
            // deviation
            // 
            this.deviation.LargeChange = 10;
            this.deviation.Location = new System.Drawing.Point(166, 87);
            this.deviation.Maximum = 25;
            this.deviation.Minimum = 1;
            this.deviation.Name = "deviation";
            this.deviation.Size = new System.Drawing.Size(229, 45);
            this.deviation.TabIndex = 12;
            this.deviation.Value = 15;
            this.deviation.ValueChanged += new System.EventHandler(this.setDeviation);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Allowed deviation";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Color data";
            // 
            // imgColor
            // 
            this.imgColor.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.imgColor.Location = new System.Drawing.Point(16, 32);
            this.imgColor.Margin = new System.Windows.Forms.Padding(15);
            this.imgColor.Name = "imgColor";
            this.imgColor.Size = new System.Drawing.Size(320, 240);
            this.imgColor.TabIndex = 24;
            this.imgColor.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 586);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Modeling";
            // 
            // imgModeling
            // 
            this.imgModeling.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.imgModeling.Location = new System.Drawing.Point(16, 601);
            this.imgModeling.Margin = new System.Windows.Forms.Padding(15);
            this.imgModeling.Name = "imgModeling";
            this.imgModeling.Size = new System.Drawing.Size(320, 240);
            this.imgModeling.TabIndex = 22;
            this.imgModeling.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 302);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "PNG image";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(378, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Depth data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(375, 302);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "TIFF image";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(375, 586);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Result output";
            // 
            // Output
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 962);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menu);
            this.Name = "Output";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Geodata";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Output_Close);
            this.Load += new System.EventHandler(this.Output_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxObstacleArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minObstacleArea)).EndInit();
            this.groupPlayer.ResumeLayout(false);
            this.groupPlayer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visibilityDeviation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOfShot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shotDispersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minPlayerArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxPlayerArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveableRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerView)).EndInit();
            this.groupSurface.ResumeLayout(false);
            this.groupSurface.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heightOfObstacle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgWMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDepth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxLowColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxHighColor)).EndInit();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupGame.ResumeLayout(false);
            this.groupGame.PerformLayout();
            this.groupModeling.ResumeLayout(false);
            this.groupModeling.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deviation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgModeling)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Emgu.CV.UI.ImageBox imgOutput;
        private System.Windows.Forms.Label lblSurface;
        private System.Windows.Forms.TrackBar modelHeight;
        private System.Windows.Forms.TrackBar maxObstacleArea;
        private System.Windows.Forms.Label lblObstacleMax;
        private System.Windows.Forms.Label lblObstacleMin;
        private System.Windows.Forms.TrackBar minObstacleArea;
        private System.Windows.Forms.GroupBox groupPlayer;
        private System.Windows.Forms.GroupBox groupSurface;
        private System.Windows.Forms.TrackBar playerView;
        private System.Windows.Forms.Label lblPlayerView;
        private System.Windows.Forms.TrackBar minPlayerArea;
        private System.Windows.Forms.TrackBar maxPlayerArea;
        private System.Windows.Forms.Label lblMinPlayerArea;
        private System.Windows.Forms.Label lblMaxPlayerArea;
        private Emgu.CV.UI.ImageBox imgWMS;
        private Emgu.CV.UI.ImageBox imgDepth;
        private Emgu.CV.UI.ImageBox imgMap;
        private System.Windows.Forms.Label txtFps;
        private System.Windows.Forms.PictureBox boxLowColor;
        private System.Windows.Forms.PictureBox boxHighColor;
        private System.Windows.Forms.Label lblLowColor;
        private System.Windows.Forms.Label lblHighColor;
        private System.Windows.Forms.ToolStrip menu;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripButton btnLoad;
        private System.Windows.Forms.ToolStripButton btnModel;
        private System.Windows.Forms.ToolStripButton btnPlay;
        private System.Windows.Forms.GroupBox groupModeling;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem btnSaveAsPng;
        private System.Windows.Forms.ToolStripMenuItem btnSaveAsTiff;
        private System.Windows.Forms.ToolStripSplitButton btnSettings;
        private System.Windows.Forms.ToolStripMenuItem bthHighlightObstacles;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripButton btnReset;
        private System.Windows.Forms.ToolStripButton btnExit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar deviation;
        private System.Windows.Forms.Label label6;
        private Emgu.CV.UI.ImageBox imgModeling;
        private System.Windows.Forms.ToolStripMenuItem btnLoadMarkers;
        private System.Windows.Forms.Label label7;
        private Emgu.CV.UI.ImageBox imgColor;
        private System.Windows.Forms.ToolStripMenuItem btnDetectPlayers;
        private System.Windows.Forms.ToolStripMenuItem btnCompareHeights;
        private System.Windows.Forms.GroupBox groupGame;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox numbersOfPlayers;
        private System.Windows.Forms.TrackBar shotDispersion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TrackBar moveableRadius;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TrackBar sizeOfShot;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TrackBar visibilityDeviation;
        private System.Windows.Forms.TrackBar heightOfObstacle;
    }
}