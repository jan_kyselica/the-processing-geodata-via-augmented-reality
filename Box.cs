﻿using System;
using System.Linq;
using System.Drawing;

namespace Geodata
{
    public class Box
    {
        public Point LeftBottom { get; set; }
        public Point RightBottom { get; set; }
        public Point LeftTop { get; set; }
        public Point RightTop { get; set; }

        public Box(Point leftBottom, Point rightBottom, Point leftTop, Point rightTop)
        {
            this.LeftBottom = leftBottom;
            this.RightBottom = rightBottom;
            this.LeftTop = leftTop;
            this.RightTop = rightTop;
        }

        public Box(PointF leftBottom, PointF rightBottom, PointF leftTop, PointF rightTop)
        {
            this.LeftBottom = Converter.PointFToPoint(leftBottom);
            this.RightBottom = Converter.PointFToPoint(rightBottom);
            this.LeftTop =  Converter.PointFToPoint(leftTop);
            this.RightTop = Converter.PointFToPoint(rightTop);
        }


        public void sort()
        {
            Point[] vertices = new Point[] {LeftBottom, RightBottom, LeftTop, RightTop};

            vertices = vertices.OrderBy(s => s.X).ThenBy(s => s.Y).ToArray();

            float x = Math.Max(vertices[1].X, vertices[0].X);
            float y = Math.Max(vertices[1].Y, vertices[2].Y);
            float width = Math.Min(vertices[2].X, vertices[3].X) - x;
            float height = Math.Min(vertices[0].Y, vertices[3].Y) - y;
        }
    }
}
