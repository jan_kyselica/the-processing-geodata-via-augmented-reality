﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace Geodata
{
    /// <summary>
    /// Class for flashing error messages
    /// </summary>
    static class Error
    {        
        public static Image<Bgr, Byte> flash(string message)
        {
            Image<Bgr, Byte> img = new Image<Bgr, Byte>(Config.ColorWidth/2, Config.ColorHeight/2, new Bgr(Color.Black));

            MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_PLAIN, 1.0, 1.0);
            img.Draw(message, ref font, new Point(80, Config.ColorHeight / 4), new Bgr(Color.Red));
            return img;        
        }
    }
}
