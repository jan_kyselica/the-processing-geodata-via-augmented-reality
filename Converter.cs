﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;


namespace Geodata
{
    /// <summary>
    /// Helper class for converting images and arrays
    /// </summary>
    public static class Converter
    {        
        public static BitmapSource ByteArrayToBitmapSource(byte[] bytes)
        {
            var stream = new MemoryStream(bytes);
            return System.Windows.Media.Imaging.BitmapFrame.Create(stream);
        }

        public static byte[] BitmapToByteArray(Bitmap bmp)
        {
            Bitmap bm = new Bitmap(bmp);
            // casting is important
            Image img = (Image)bm;   
            byte[] data = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {                
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                stream.Close();
                data = stream.ToArray();                
            }

            return data;            
        }

        public static byte[] BitmapToByteArrayWithoutHeader(Bitmap bmp)
        {
            byte[] data = Converter.BitmapToByteArray(bmp);
            byte[] imgData = new byte[data.Length - 54];
            Array.Copy(data, 54, imgData, 0, imgData.Length);
            return imgData;
        }

        public static Bitmap ColorImageToBitmap(ColorImageFrame Image)
        {
            byte[] pixeldata = new byte[Image.PixelDataLength];
            Image.CopyPixelDataTo(pixeldata);
            Bitmap bmap = new Bitmap(Image.Width, Image.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            BitmapData bmapdata = bmap.LockBits(
                new Rectangle(0, 0, Image.Width, Image.Height),
                ImageLockMode.WriteOnly,
                bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            Marshal.Copy(pixeldata, 0, ptr, Image.PixelDataLength);
            bmap.UnlockBits(bmapdata);
            return bmap;
        }

        public static Bitmap ColorImageToBitmap(DepthImageFrame Image)
        {
            short[] pixelData = new short[Image.PixelDataLength];
            Image.CopyPixelDataTo(pixelData);
            for (int i = 0; i < Image.PixelDataLength; i++)
            {
                pixelData[i] = (short)(((ushort)pixelData[i]) >> 2);                   
            }

            Bitmap bmap = new Bitmap(Image.Width, Image.Height, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            BitmapData bmapdata = bmap.LockBits(new Rectangle(0, 0, Image.Width, Image.Height), ImageLockMode.WriteOnly, bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            Marshal.Copy(pixelData, 0, ptr, Image.Width * Image.Height);
            bmap.UnlockBits(bmapdata);
            return bmap;
        }

        // used for converting colorDepthData to bitmap 
        public static Bitmap ByteArrayToBitmap(byte[] colorData, int dataLength)
        {
            Bitmap bmap = new Bitmap(Config.DepthWidth, Config.DepthHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            BitmapData bmapdata = bmap.LockBits(
                new Rectangle(0, 0, Config.DepthWidth, Config.DepthHeight),
                ImageLockMode.WriteOnly,
                bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            Marshal.Copy(colorData, 0, ptr, dataLength);
            bmap.UnlockBits(bmapdata);
            return bmap;
        }       

        public static System.Drawing.Point PointFToPoint(PointF p)
        {
            return new System.Drawing.Point((int)p.X, (int)p.Y);
        }       
    }
}
